#include "spi_flash.h"


/*******************************************************************************
 * Variables
 ******************************************************************************/
unsigned char masterRxData1[TRANSFER_SIZE1] = {0};
unsigned char masterTxData1[TRANSFER_SIZE1] = {0}; 

dspi_master_edma_handle_t g_dspi1_edma_m_handle;
edma_handle_t dspiEdmaMasterRxRegToRxDataHandle;
edma_handle_t dspiEdmaMasterIntermediaryToTxRegHandle;
#if (!(defined(FSL_FEATURE_DSPI_HAS_GASKET) && FSL_FEATURE_DSPI_HAS_GASKET))
edma_handle_t dspiEdmaMasterTxDataToIntermediaryHandle;
#endif

volatile unsigned char isTransferCompleted  = 1;

/*******************************************************************************
 * Code
 ******************************************************************************/
static void DSPI_MasterUserCallback(SPI_Type *base, dspi_master_edma_handle_t *handle, status_t status, void *userData)
{
	if (status == kStatus_Success)
	{
		isTransferCompleted = 1;
	}
    else
    {
        isTransferCompleted = 2;
    }
}

static void DSPI_Transfer(unsigned char* txdata,unsigned char* rxdata,unsigned short datasize)
{
    dspi_transfer_t masterXfer;
    /* Start master transfer, send data to slave */
    isTransferCompleted    = 0;
    masterXfer.txData      = txdata;
    masterXfer.rxData      = rxdata;
    masterXfer.dataSize    = datasize;
    masterXfer.configFlags = kDSPI_MasterCtar0 | EXAMPLE_DSPI_MASTER_PCS_FOR_TRANSFER | kDSPI_MasterPcsContinuous;  //kDSPI_MasterCtar0 | EXAMPLE_DSPI_MASTER_PCS_FOR_TRANSFER | 
    DSPI_MasterTransferEDMA(EXAMPLE_DSPI_MASTER_BASEADDR, &g_dspi1_edma_m_handle, &masterXfer);
}

//unsigned int tk1 = 2000;
static void W25QXXX_wait_trans(unsigned short cnt)
{
    if(cnt == 0) return;
    while(!isTransferCompleted)
    {       
        DelayMs(1);
        cnt--;
        if(cnt == 0)
            break;        
    }
//    if(tk1 > cnt)
//      tk1 = cnt;
}

//读取W25QXX的状态寄存器，W25QXX一共有3个状态寄存器
static unsigned char W25QXXX_ReadSR(void)   
{  
    isTransferCompleted = 0;
    masterTxData1[0] = W25X_ReadStatusReg1;
    masterTxData1[1] = 0xff;
    DSPI_Transfer(masterTxData1, masterRxData1, 2);
    W25QXXX_wait_trans(1000);
    if(!isTransferCompleted)  
        return 0x01;    
    return masterRxData1[1];   
}

//等待空闲
static unsigned char W25QXXX_Wait_Busy(void)   
{ 
    unsigned short waitcount = 5000;    
	while(1)
	{
        DelayMs(1);
        if( !(W25QXXX_ReadSR()&0x01) ) break;
        
        waitcount--;
        if(waitcount == 0)
            break;
	}
    return waitcount;
}

//unsigned int tk3 = 7000;
static unsigned char W25QXXX_Wait_WriteEnable(void)   
{ 
    unsigned short waitcount = 5000;    
	while(1)
	{
        DelayMs(1);
        if( W25QXXX_ReadSR()&0x02 ) break;
        
        waitcount--;
        if(waitcount == 0)
            break;
	}
//    if(tk3 > waitcount) 
//      tk3 = waitcount;    
    return waitcount;
}


unsigned short W25QXXX_ReadID(void)
{
    unsigned short temp = 0;
    
    masterTxData1[0] = 0x90;
    masterTxData1[1] = 0x00;
    masterTxData1[2] = 0x00;
    masterTxData1[3] = 0x00;
    masterTxData1[4] = 0x11;
    masterTxData1[5] = 0x11;
    
    DSPI_Transfer(masterTxData1, masterRxData1, 6);
    if( W25QXXX_Wait_Busy() )
    {
        temp |= masterRxData1[4]<<8;
        temp |= masterRxData1[5];
        return temp;
    }
    return 0;
}

static void W25QXXX_WriteEnable()
{
    masterTxData1[0] = W25X_WriteEnable;
    DSPI_Transfer(masterTxData1, masterRxData1, 1);
    W25QXXX_Wait_WriteEnable();
}

//static void W25QXXX_WriteDisable()
//{
//    masterTxData1[0] = W25X_WriteDisable;
//    DSPI_Transfer(masterTxData1, masterRxData1, 1);
//}

void DSPI_Flash_Init(void)
{    
    /* Port B Clock Gate Control: Clock enabled */
    CLOCK_EnableClock(kCLOCK_PortB);

    /* PORTB10 (pin 58) is configured as SPI1_PCS0 */
    PORT_SetPinMux(PORTB, 10U, kPORT_MuxAlt2);
    
    /* PORTB11 (pin 59) is configured as SPI1_SCK */
    PORT_SetPinMux(PORTB, 11U, kPORT_MuxAlt2);

    /* PORTB16 (pin 62) is configured as SPI1_SOUT */
    PORT_SetPinMux(PORTB, 16U, kPORT_MuxAlt2);

    /* PORTB17 (pin 63) is configured as SPI1_SIN */
    PORT_SetPinMux(PORTB, 17U, kPORT_MuxAlt2);

    uint32_t srcClock_Hz;
    dspi_master_config_t masterConfig;
    /* Master config */
    masterConfig.whichCtar                                = kDSPI_Ctar0;
    masterConfig.ctarConfig.baudRate                      = TRANSFER_BAUDRATE;
    masterConfig.ctarConfig.bitsPerFrame                  = 8U;
    masterConfig.ctarConfig.cpol                          = kDSPI_ClockPolarityActiveLow;
    masterConfig.ctarConfig.cpha                          = kDSPI_ClockPhaseSecondEdge;
    masterConfig.ctarConfig.direction                     = kDSPI_MsbFirst;
    masterConfig.ctarConfig.pcsToSckDelayInNanoSec        = 1000000000U / TRANSFER_BAUDRATE;
    masterConfig.ctarConfig.lastSckToPcsDelayInNanoSec    = 1000000000U / TRANSFER_BAUDRATE;
    masterConfig.ctarConfig.betweenTransferDelayInNanoSec = 1000000000U / TRANSFER_BAUDRATE;

    masterConfig.whichPcs           = EXAMPLE_DSPI_MASTER_PCS_FOR_INIT;
    masterConfig.pcsActiveHighOrLow = kDSPI_PcsActiveLow;

    masterConfig.enableContinuousSCK        = false;
    masterConfig.enableRxFifoOverWrite      = false;
    masterConfig.enableModifiedTimingFormat = false;
    masterConfig.samplePoint                = kDSPI_SckToSin0Clock;

    srcClock_Hz = DSPI_MASTER_CLK_FREQ;
    DSPI_MasterInit(EXAMPLE_DSPI_MASTER_BASEADDR, &masterConfig, srcClock_Hz);

    /* DMA Mux setting and EDMA init */
    unsigned int masterRxChannel, masterTxChannel;
//    edma_config_t userConfig;

    masterRxChannel = DMA_SPI_FLASH_RX_CHANNEL;
    masterTxChannel = DMA_SPI_FLASH_TX_CHANNEL;
    
    /* If DSPI instances support Gasket feature, only two channels are needed. */
#if (!(defined(FSL_FEATURE_DSPI_HAS_GASKET) && FSL_FEATURE_DSPI_HAS_GASKET))
    unsigned int masterIntermediaryChannel;
    masterIntermediaryChannel = DMA_SPI_FLASH_INTERMEDIA_CHANNEL;
#endif

    /* DMA MUX init */
//    DMAMUX_Init(EXAMPLE_DSPI_MASTER_DMA_MUX_BASEADDR);

    DMAMUX_SetSource(EXAMPLE_DSPI_MASTER_DMA_MUX_BASEADDR, masterRxChannel, EXAMPLE_DSPI_MASTER_DMA_RX_REQUEST_SOURCE);
    DMAMUX_EnableChannel(EXAMPLE_DSPI_MASTER_DMA_MUX_BASEADDR, masterRxChannel);

    DMAMUX_SetSource(EXAMPLE_DSPI_MASTER_DMA_MUX_BASEADDR, masterTxChannel, EXAMPLE_DSPI_MASTER_DMA_TX_REQUEST_SOURCE);
    DMAMUX_EnableChannel(EXAMPLE_DSPI_MASTER_DMA_MUX_BASEADDR, masterTxChannel);

    /* EDMA init */
    /*
     * userConfig.enableRoundRobinArbitration = false;
     * userConfig.enableHaltOnError = true;
     * userConfig.enableContinuousLinkMode = false;
     * userConfig.enableDebugMode = false;
     */
//    EDMA_GetDefaultConfig(&userConfig);
//    EDMA_Init(EXAMPLE_DSPI_MASTER_DMA_BASEADDR, &userConfig);

    /* Set up dspi master */
    memset(&(dspiEdmaMasterRxRegToRxDataHandle), 0, sizeof(dspiEdmaMasterRxRegToRxDataHandle));
    memset(&(dspiEdmaMasterIntermediaryToTxRegHandle), 0, sizeof(dspiEdmaMasterIntermediaryToTxRegHandle));
#if (!(defined(FSL_FEATURE_DSPI_HAS_GASKET) && FSL_FEATURE_DSPI_HAS_GASKET))
    memset(&(dspiEdmaMasterTxDataToIntermediaryHandle), 0, sizeof(dspiEdmaMasterTxDataToIntermediaryHandle));
#endif
    

    EDMA_CreateHandle(&(dspiEdmaMasterRxRegToRxDataHandle), EXAMPLE_DSPI_MASTER_DMA_BASEADDR, masterRxChannel);
    EDMA_CreateHandle(&(dspiEdmaMasterIntermediaryToTxRegHandle), EXAMPLE_DSPI_MASTER_DMA_BASEADDR, masterTxChannel);
#if (!(defined(FSL_FEATURE_DSPI_HAS_GASKET) && FSL_FEATURE_DSPI_HAS_GASKET))
    EDMA_CreateHandle(&(dspiEdmaMasterTxDataToIntermediaryHandle), EXAMPLE_DSPI_MASTER_DMA_BASEADDR,
                      masterIntermediaryChannel);
#endif
    

#if (defined(FSL_FEATURE_DSPI_HAS_GASKET) && FSL_FEATURE_DSPI_HAS_GASKET)
    DSPI_MasterTransferCreateHandleEDMA(EXAMPLE_DSPI_MASTER_BASEADDR, &g_dspi1_edma_m_handle, DSPI_MasterUserCallback,
                                        NULL, &dspiEdmaMasterRxRegToRxDataHandle, NULL,
                                        &dspiEdmaMasterIntermediaryToTxRegHandle);
#else
    DSPI_MasterTransferCreateHandleEDMA(EXAMPLE_DSPI_MASTER_BASEADDR, &g_dspi1_edma_m_handle, DSPI_MasterUserCallback,
                                        NULL, &dspiEdmaMasterRxRegToRxDataHandle,
                                        &dspiEdmaMasterTxDataToIntermediaryHandle,
                                        &dspiEdmaMasterIntermediaryToTxRegHandle);
#endif   
}

void W25QXXX_Erase_Sector(unsigned int destAddr)   
{  
    W25QXXX_WriteEnable();

    masterTxData1[0] = W25X_SectorErase;
    masterTxData1[1] = (unsigned char)((destAddr)>>16);
    masterTxData1[2] = (unsigned char)((destAddr)>>8);
    masterTxData1[3] = (unsigned char)destAddr;
    DSPI_Transfer(masterTxData1, masterRxData1, 4);
    W25QXXX_Wait_Busy();
}

void W25QXXX_Erase_Block64(unsigned int destAddr)   
{  
    W25QXXX_WriteEnable();

    masterTxData1[0] = W25X_BlockErase;
    masterTxData1[1] = (unsigned char)((destAddr)>>16);
    masterTxData1[2] = (unsigned char)((destAddr)>>8);
    masterTxData1[3] = (unsigned char)destAddr;
    DSPI_Transfer(masterTxData1, masterRxData1, 4);
    W25QXXX_Wait_Busy();
}

unsigned short W25QXXX_Read_Page(unsigned char* pBuffer, unsigned int destAddr, unsigned short NumByteToRead)   
{
    masterTxData1[0] = W25X_ReadData;
    masterTxData1[1] = (unsigned char)((destAddr)>>16);
    masterTxData1[2] = (unsigned char)((destAddr)>>8);
    masterTxData1[3] = (unsigned char)destAddr; 
    for(int i=0;i<NumByteToRead;i++)
    {
        masterTxData1[4+i] = 0xFF;
    }
    DSPI_Transfer(masterTxData1, masterRxData1, 4+NumByteToRead);
    if(!W25QXXX_Wait_Busy()) return 0;

    for(int i=0;i<NumByteToRead;i++)
      *pBuffer++ = masterRxData1[4+i];
    return NumByteToRead;
}

//写一页
void W25QXXX_Write_Page(unsigned char* pBuffer, unsigned int destAddr, unsigned short NumByteToWrite)
{
    W25QXXX_WriteEnable();

    masterTxData1[0] = W25X_PageProgram;
    masterTxData1[1] = (unsigned char)((destAddr)>>16);
    masterTxData1[2] = (unsigned char)((destAddr)>>8);
    masterTxData1[3] = (unsigned char)destAddr; 
    for(int i=0;i<NumByteToWrite;i++)
	    masterTxData1[4+i] = (*pBuffer++); 

    DSPI_Transfer(masterTxData1, masterRxData1, 4+NumByteToWrite);
    W25QXXX_Wait_Busy();
}