/*********************************************************************
* Copyright  CGXi Intelligent Manufacturing Wuxi Ltd.
* File       board.h
* Brief 	                                         
* --------------------------------------------------------------------
* | Date	    | Version	| Author     | Description
* | 		    |           |            |
* |             |           |            |
* | 2022-06-30  | 1.0.0		| RuiCong    | Create File
**********************************************************************/





#ifndef BOARD_H
#define BOARD_H
/**************************************** Other conditional compilation options ****************************/


/**************************************** Standard library header file *************************************/
//#include <xxx.h>


/**************************************** Non-standard library header file *********************************/
#include "baseDef.h"
#include "boardCfg.h"
#include "main.h"

#include "fsl_common.h"
#include "fsl_adc16.h"		/* MS0082/ADXL325	*/
#include "fsl_clock.h"
#include "fsl_crc.h"
#include "fsl_dmamux.h"
#include "fsl_edma.h"
#include "fsl_flash.h"
#include "fsl_ftfx_flash.h"
#include "fsl_ftm.h"
#include "fsl_gpio.h"
#include "fsl_pdb.h"
#include "fsl_pit.h"
#include "fsl_port.h"
#include "fsl_rcm.h"
#include "fsl_sim.h"
#include "fsl_smc.h"
#include "fsl_uart.h"
#include "fsl_uart_edma.h"
#include "fsl_xbara.h"
#include "fsl_wdog.h"

#include "bsp_i2c.h"
#include "bsp_uart.h"



/*MACRO DEFINITIONS FOR UART0 PINS*/
#define PORT_UART0_RX PORTB
#define PIN_UART0_RX  0U
#define PORT_UART0_TX PORTB
#define PIN_UART0_TX  1U
#define SOPT5_UART0RXSRC_UART_RX 0x00u /*!<@brief UART 0 receive data source select: UART0_RX pin */
#define SOPT5_UART0TXSRC_UART_TX 0x00u /*!<@brief UART 0 transmit data source select: UART0_TX pin */

/*MACRO DEFINITIONS FOR CAN0 PINS*/
#define PORT_CAN0_TX PORTB
#define PIN_CAN0_TX  18U
#define PORT_CAN0_RX PORTB
#define PIN_CAN0_RX  19U

/*MACROS DEFINITIONS FOR CANTransciver TJA1043 PINS*/
#define PORT_CANTRANS_ERR        PORTD
#define GPIO_CANTRANS_ERR        GPIOD
#define PIN_CANTRANS_ERR         4U
#define PORT_CANTRANS_EN         PORTD
#define GPIO_CANTRANS_EN         GPIOD
#define PIN_CANTRANS_EN          5U
#define PORT_CANTRANS_KL15       PORTD
#define GPIO_CANTRANS_KL15       GPIOD
#define PIN_CANTRANS_KL15        6U
#define PORT_CANTRANS_STB        PORTD
#define GPIO_CANTRANS_STB        GPIOD
#define PIN_CANTRANS_STB         7U
#define PORT_CANTRANS_KL15_CTRL  PORTE
#define GPIO_CANTRANS_KL15_CTRL  GPIOE
#define PIN_CANTRANS_KL15_CTRL   2U

/*--------------------------------------------------------------------------------------------------*/
/*	For mcu clock config																			*/
/*--------------------------------------------------------------------------------------------------*/
#define BOARD_BOOTCLOCKRUN_CORE_CLOCK		120000000U	    /*!< Core clock frequency: 120000000Hz	*/
#define BOARD_BOOTCLOCKVLPR_CORE_CLOCK		4000000U	    /*!< Core clock frequency: 4000000Hz	*/
#define BOARD_BOOTCLOCKHSRUN_CORE_CLOCK 	240000000U	    /*!< Core clock frequency: 240MHz		*/
#define BOARD_BUS_CLOCK_FREQ_KHZ			(20000u)

#define CAN_INSTANCE                    0
#define CAN_DEFAULT_BAUDRATE            500000u


// Send
#define MSG_CAN_DIAG_RSP                0x778

// Recv
#define MSG_CAN_DIAG_FUNC               0x7DF
#define MSG_CAN_DIAG_REQ                0x718

#define MSG_CAN_RECV_MAX                2

#define WDOG_WCT_INSTRUCITON_COUNT          (256U)
#define WDOG_PERIOD_MS                      (5000)



static unsigned int MSG_CAN_RECV_ID[MSG_CAN_RECV_MAX] = {
                                        					MSG_CAN_DIAG_FUNC,
															MSG_CAN_DIAG_REQ
														};

/**************************************** Type definitions *************************************************/
/*--------------------------------------------------------------------------------------------------*/
/*	Bootloader hold data            																*/
/*--------------------------------------------------------------------------------------------------*/
enum{
    ECU_UPDATA_IDLE,
    ECU_UPDATA_RECV,    
    ECU_UPDATA_READY,
    ECU_UPDATA_PROG_GOON,
    ECU_UPDATA_PROG,
    ECU_UPDATA_FINISH,
    ECU_UPDATA_FAIL
};
	
typedef struct _BOOT_HOLD_DATA
{
    uint32  VendorID;
    uint32  ProductCode;
    uint32  Revision;
    uint32  SerialNumber;
    uint16  HardwareVersion;
    uint16  BootJump;
    uint16  ResetCnt1;
    uint16  ResetCnt2;
    uint32  Reserve1;
    uint32  Reserve2;
}BOOT_HOLD_DATA;

typedef struct __SYSCLK_PARA
{
	unsigned int					CoreSysClk;
	unsigned int					FastPeriphClk;
    unsigned int                    FlexBusClk;
	unsigned int					BusClk;
    unsigned int                    FlashClk;
}SYSCLK_PARA;


typedef struct __MEM_MONITOR
{
    unsigned short          UsedPercent;
    unsigned short          UsedIndex;
    int*                    Percent[11];
}MEM_MONITOR;

/**************************************** Structure definitions ********************************************/


/**************************************** Data type ********************************************************/


/**************************************** Function declaration *********************************************/

/**
 * @brief Gets the Watchdog timer output.
 *
 * @param base WDOG peripheral base address
 * @return Current value of watchdog timer counter.
 */
static inline unsigned int GetWdogTimer(WDOG_Type *base)
{
    return (unsigned int)((((unsigned int)base->TMROUTH) << 16U) | (base->TMROUTL));
}

void BOARD_InitHoldData();
uint8 BOARD_InitFlash();
void BOARD_InitRAM();
void BOARD_BootClockHSRUN();
void BOARD_BootClockRUN();
void BOARD_InitPins();
void BOARD_ConfigDMA();
void BOARD_InitSysClk();
void BOARD_ConfigIrqPriority();
void BOARD_ConfigBootTimer(int32_t us);
void BOARD_InitCAN(void);
void BoardConfigure();
void BOARD_KL15Sleep(void);



#endif /* XXX_H */






