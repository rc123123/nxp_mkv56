#ifndef _BSP_I2C1_TEMP175_H
#define _BSP_I2C1_TEMP175_H

#include "bsp_i2c.h"


#define I2C1_SLAVE_ADDRESS_WRITE     (0x90)    //1001 000(0)
#define I2C1_SLAVE_ADDRESS_READ      (0x91)    //1001 000(1)

#if defined(__cplusplus)
extern "C" {
#endif /*_cplusplus. */

xbool TEMP175_ReadTemperature(xfloat *currentTemperature);

/* @} */
#if defined(__cplusplus)
}
#endif /*_cplusplus. */
/*@}*/

#endif