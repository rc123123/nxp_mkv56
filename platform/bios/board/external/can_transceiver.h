/*********************************************************************
* Copyright  CGXi Intelligent Manufacturing Wuxi Ltd.
* File       can_transceiver.h
* Brief 	                                         
* --------------------------------------------------------------------
* | Date	    | Version	| Author     | Description
* | 		    |           |            |
* |             |           |            |
* | 2022-11-11  | 1.0.0		| RuiCong    | Create File
**********************************************************************/

#ifndef _CAN_TRANSCEIVER_H_
#define _CAN_TRANSCEIVER_H_

#if defined(__cplusplus)
extern "C"{
#endif

    
#include <MKV56F24.h>




#define     PROT_CAN_ERR       PORTD
#define     PROT_CAN_EN        PORTD
#define     PORT_CAN_KL15      PORTD
#define     PROT_CAN_STB       PORTD

#define     GPIO_CAN_ERR       GPIOD
#define     GPIO_CAN_EN        GPIOD
#define     GPIO_CAN_KL15	   GPIOD
#define     GPIO_CAN_STB       GPIOD

#define     PIN_CAN_ERR        4U
#define     PIN_CAN_EN         5U
#define 	PIN_CAN_KL15	   6U
#define     PIN_CAN_STB        7U




void CANTRANS_GpioInit(void);
void CANTRANS_CyclicDetect(void);
    
    
#if defined(__cplusplus)
}
#endif

#endif