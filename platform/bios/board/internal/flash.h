#ifndef FLASH_H
#define FLASH_H

#include "Basedef.h"
#include "board.h"

unsigned char ProgramFlash(unsigned char* Src, unsigned int Dest, unsigned short Bytes);
void ProgramFlashErase(unsigned int Dest);
unsigned char ProgramFlashSector(unsigned char* Src, unsigned int Dest);

#endif