/****************************************************************************************************/
/*																									*/
/*																									*/
/*	Basedef.h : Basic Data Type Definition Header File.												*/
/*																									*/
/*																									*/
/****************************************************************************************************/
/*																									*/
/*	Function	: 																					*/
/*																									*/
/********************** Copyright (C) CGX Co., Ltd **************************************************/
/*																									*/
/*	Note		:	20181108 ZhuYuanbo: Create file													*/
/*																									*/
/****************************************************************************************************/
#ifndef _BASEDEF_H_
#define _BASEDEF_H_

/****************************************************************************************************/
/*	Only valid when use Kinetis cpu																	*/
/****************************************************************************************************/
#define EnableInterrupts		asm(" CPSIE i")
#define DisableInterrupts		asm(" CPSID i")
/****************************************************************************************************/
/*																									*/
/*		Basic Data Type Definition																	*/
/*																									*/
/****************************************************************************************************/
/*--------------------------------------------------------------------------------------------------*/
/*		Basic Data Type Definition																	*/
/*--------------------------------------------------------------------------------------------------*/
typedef unsigned char			uint8;     	/*  8 bits */
typedef unsigned short int		uint16;    	/* 16 bits */
typedef unsigned long int		uint32;    	/* 32 bits */
typedef unsigned long long		uint64;    	/* 64 bits */
typedef char					int8;      	/*  8 bits */
typedef short int				int16;     	/* 16 bits */
typedef int						int32;     	/* 32 bits */
typedef long long				int64;     	/* 64 bits */
typedef volatile int8			vint8;     	/*  8 bits */
typedef volatile int16			vint16;    	/* 16 bits */
typedef volatile int32			vint32;    	/* 32 bits */
typedef volatile int64			vint64;    	/* 64 bits */
typedef volatile uint8			vuint8;		/*  8 bits */
typedef volatile uint16			vuint16;	/* 16 bits */
typedef volatile uint32			vuint32;	/* 32 bits */
typedef volatile uint64			vuint64;	/* 64 bits */

/*--------------------------------------------------------------------------------------------------*/
typedef unsigned char       xbool;
typedef unsigned char       xint8u; 
typedef signed   char       xint8s;  
typedef unsigned short      xint16u; 
typedef signed   short      xint16s; 
typedef unsigned int        xint32u;  
typedef signed   int        xint32s; 
typedef float               xfloat; 
typedef double              xdouble;
typedef unsigned long long  xint64u;
typedef unsigned long long  xint64s;
typedef unsigned int        xsize_t;
/*--------------------------------------------------------------------------------------------------*/

typedef unsigned char			BOOL;
typedef signed char				CHAR;
typedef unsigned char			UCHAR;
typedef short					SHORT;
typedef unsigned short			USHORT;
typedef long					LONG;
typedef unsigned long			ULONG;
typedef int						INT;
typedef unsigned int			UINT;
/*--------------------------------------------------------------------------------------------------*/
typedef signed char				INT8;
typedef unsigned char			UINT8;
typedef short					INT16;
typedef unsigned short			UINT16;
typedef long					INT32;
typedef unsigned long			UINT32;
typedef long long				INT64;
typedef unsigned long long		UINT64;
/*--------------------------------------------------------------------------------------------------*/
typedef volatile signed char	VCHAR;
typedef volatile unsigned char	VUCHAR;
typedef volatile short			VSHORT;
typedef volatile unsigned short	VUSHORT;
typedef volatile long			VLONG;
typedef volatile unsigned long	VULONG;
typedef volatile int			VINT;
typedef volatile unsigned int	VUINT;
/*--------------------------------------------------------------------------------------------------*/
typedef	const UCHAR				CUCHAR;
typedef	const USHORT			CUSHORT;
typedef	const ULONG				CULONG;
/*--------------------------------------------------------------------------------------------------*/
typedef	long					KMGAIN;				/* {k,s} Type for Mulgain()						*/
typedef	long					KSGAIN;				/* {k,s} Type for PcalKsksks()					*/
/*--------------------------------------------------------------------------------------------------*/
/*		Double Byte Data Type Definition															*/
/*--------------------------------------------------------------------------------------------------*/
#pragma pack(2)
typedef union
{
    USHORT	w;										/* Word											*/
    struct{ UCHAR l; UCHAR h;} b;					/* Low Byte / High Byte							*/
/*--------------------------------------------------------------------------------------------------*/
	SHORT	Short;									/* signed   short								*/
	USHORT	Ushort;									/* unsigned short								*/
/*--------------------------------------------------------------------------------------------------*/
	CHAR	Char[2];								/* signed   char								*/
	UCHAR	Uchar[2];								/* unsigned char								*/
} DBYTEX;
#pragma pack( )
/*--------------------------------------------------------------------------------------------------*/
/*		Double Word Data Type Definition															*/
/*--------------------------------------------------------------------------------------------------*/
typedef	union
{
	ULONG	dw;										/* Double Word									*/
	struct{ USHORT l; USHORT h;} w;					/* Low Word / High Word							*/
/*--------------------------------------------------------------------------------------------------*/
	LONG	Long;									/* signed   long								*/
	ULONG	Ulong;									/* unsigned long								*/
/*--------------------------------------------------------------------------------------------------*/
	SHORT	Short[2];								/* signed   short								*/
	USHORT	Ushort[2];								/* unsigned short								*/
/*--------------------------------------------------------------------------------------------------*/
	CHAR	Char[4];								/* signed   char								*/
	UCHAR	Uchar[4];								/* unsigned char								*/
} DWORDX;
/****************************************************************************************************/
/*																									*/
/*		Basic Constant Definition																	*/
/*																									*/
/****************************************************************************************************/
//typedef enum { NG=-1,   OK=0   } OKNGX;
//typedef enum { FALSE=0, TRUE=1 } BOOLX;

#ifdef		FALSE
#undef		FALSE
#endif
#define		FALSE						(0)

#ifdef		TRUE
#undef		TRUE
#endif
#define		TRUE						(1)

#ifdef  LO
#undef  LO
#endif
#define LO                              (0)

#ifdef  HI
#undef  HI
#endif
#define HI                              (1)
/*--------------------------------------------------------------------------------------------------*/
#define		ON							1
#define		OFF							0
/*--------------------------------------------------------------------------------------------------*/
#define     PIN_HIGH                    1
#define     PIN_LOW                     0
#define     PIN_HIGH_Z                  0xFF
/*--------------------------------------------------------------------------------------------------*/
#define		BUSY			 			1
#define		FREE			 			0
/*--------------------------------------------------------------------------------------------------*/
#define		WAIT			 			1
#define		NOWAIT						0
/*--------------------------------------------------------------------------------------------------*/
#ifndef		NULL
#define		NULL						((void *) 0)
#endif
/*--------------------------------------------------------------------------------------------------*/
#define		KSPAI						0x156487ED			/* {k,s} Type   PAI						*/
#define		KS2PAI						0x146487ED			/* {k,s} Type 2*PAI						*/
#define		KS3PAI						0x134B65F2			/* {k,s} Type 3*PAI						*/
#define		KS4PAI						0x136487ED			/* {k,s} Type 4*PAI						*/
/*--------------------------------------------------------------------------------------------------*/
#define		NORMAXVALUE					0x01000000			/* Normalized Maximum Value				*/
#define		NORMINVALUE					-0x01000000			/* Normalized Minimum Value				*/
/*--------------------------------------------------------------------------------------------------*/
/*	Unit trans coef																					*/
/*--------------------------------------------------------------------------------------------------*/
#define _C_RPM_RADPS					(0.10471975512f)
#define _C_DEGREE_RAD					(0.01745329252f)
#define _C_01A_A						(0.01f)
#define _C_MNM_NM						(0.001f)
/*--------------------------------------------------------------------------------------------------*/
#define CTRL_RESETING					0					/* Indicate object need be reset		 */
#define CTRL_RESETED					1
/*--------------------------------------------------------------------------------------------------*/
/*	Servo on source																					*/
/*--------------------------------------------------------------------------------------------------*/
#define EN_SIGNAL_SOFF					(0u)
#define EN_SIGNAL_UART					(1u)
#define EN_SIGNAL_ECAT					(2u)
/*--------------------------------------------------------------------------------------------------*/
/*	Open control mode																				*/
/*--------------------------------------------------------------------------------------------------*/
#define CTRL_OPEN_OFF					(0x00)
#define CTRL_OPEN_ANGLE_OUT				(0x01)
#define CTRL_OPEN_ANGLE_FB				(0x02)
#define CTRL_OPEN_ANGLE_BOTH			(0x03)
#define CTRL_OPEN_VOLT					(0x04)
#define CTRL_OPEN_VOLT_ANGLE_OUT		(0x05)
#define CTRL_OPEN_VOLT_ANGLE_FB			(0x06)
#define CTRL_OPEN_VOLT_ANGLE_BOTH		(0x07)
/*--------------------------------------------------------------------------------------------------*/
/*	Normal control mode																				*/
/*--------------------------------------------------------------------------------------------------*/
#define CTRL_MODE_NONE					(0)
#define CTRL_MODE_POS					(1)
#define CTRL_MODE_VEL					(2)
#define CTRL_MODE_TRQ					(3)
#define CTRL_MODE_OPENLOOP				(4)
#define CTRL_MODE_DB					(5)

#define CTRL_MODE_UI_ECAT				(0xF)
#define CTRL_MODE_UI_NONE				(0x0)
#define CTRL_MODE_UI_POS				(0x1)
#define CTRL_MODE_UI_VEL				(0x2)
#define CTRL_MODE_UI_TRQ				(0x3)
#define CTRL_MODE_UI_OPENLOOP			(0x4)

#define CTRL_MODE_ECAT_NONE				(0)
#define CTRL_MODE_ECAT_PP				(1)
#define CTRL_MODE_ECAT_VL				(2)
#define CTRL_MODE_ECAT_PV				(3)
#define CTRL_MODE_ECAT_TQ				(4)
#define CTRL_MODE_ECAT_HM				(6)
#define CTRL_MODE_ECAT_IP				(7)
#define CTRL_MODE_ECAT_CSP				(8)
#define CTRL_MODE_ECAT_CSV				(9)
#define CTRL_MODE_ECAT_CST				(10)
#define CTRL_MODE_ECAT_CSTCA			(11)
/*--------------------------------------------------------------------------------------------------*/
/*	Reference source config																			*/
/*--------------------------------------------------------------------------------------------------*/
#define REF_SRC_ECAT					(0u)	/* Get reference form EtherCAT stack				*/
#define REF_SRC_MODBUS					(1u)	/* Get reference form Modbus stack					*/
#define REF_SRC_PJOG					(2u)	/* Get reference form PJOG module					*/
#define REF_SRC_SIGNAL					(3u)	/* Get reference form signal generator module		*/
#define REF_SRC_CONST					(4u)	/* Get reference form system set constant value		*/
/*--------------------------------------------------------------------------------------------------*/
/*	Position feedback encoder select																*/
/*--------------------------------------------------------------------------------------------------*/
#define POS_FEED_ENCO_R					(0u)
#define POS_FEED_ENCO_M					(1u)
/*--------------------------------------------------------------------------------------------------*/
/*	Motor encoder compensation mode																	*/
/*--------------------------------------------------------------------------------------------------*/
#define ENCOM_COMPMODE_NONE				(0u)
#define ENCOM_COMPMODE_STATIC_POS		(1u)
/*--------------------------------------------------------------------------------------------------*/
/*	Velocity feedback encoder select																*/
/*--------------------------------------------------------------------------------------------------*/
#define VEL_FEED_ENCO_M					(0u)
#define VEL_FEED_ENCO_R					(1u)
#define VEL_FEED_HYBRID					(2u)
/*--------------------------------------------------------------------------------------------------*/
/*	Control mode group																				*/
/*--------------------------------------------------------------------------------------------------*/
#define CTRLMODE_GROUP_MBUS				(0u)
#define CTRLMODE_GROUP_ECAT				(1u)
#define CTRLMODE_GROUP_INTERNAL			(2u)
/*--------------------------------------------------------------------------------------------------*/
/*	Torque reference limit group																	*/
/*--------------------------------------------------------------------------------------------------*/
#define TREF_LMT_GROUP_NORMAL			(0u)		/* 												*/
#define TREF_LMT_GROUP_EMGSTOP			(1u)		/* Destroy torque of reducer, emergency stop	*/
#define TREF_LMT_GROUP_ECAT				(2u)		/* 												*/

/****************************************************************************************************/
/*																									*/
/*		Basic Macro Definition																		*/
/*																									*/
/****************************************************************************************************/
#define	LOBYTEX( x )					(((UCHAR*)(&(x)))[0])
#define	HIBYTEX( x )					(((UCHAR*)(&(x)))[1])
/*--------------------------------------------------------------------------------------------------*/
#define	LOWORDX( x )					(((USHORT*)(&(x)))[0])
#define	HIWORDX( x )					(((USHORT*)(&(x)))[1])
/*--------------------------------------------------------------------------------------------------*/
#define	LONGOF( l, h )					((LONG)((USHORT)(l) + ((SHORT)(h) << 16)))
#define	ULONGOF( l, h )					((ULONG)((USHORT)(l) + ((ULONG)(h) << 16)))
/*--------------------------------------------------------------------------------------------------*/
/*		Bit Operation Macro Definition																*/
/*--------------------------------------------------------------------------------------------------*/
#define	BITSET( Data, BitNo )			{(Data) |=  (1 << (BitNo));}
#define	BITCLR( Data, BitNo )			{(Data) &= ~(1 << (BitNo));}
#define	BITCHK( Data, BitNo )			(((Data) & (1 << (BitNo))) != 0)
/*--------------------------------------------------------------------------------------------------*/
#define	GET_BITSDATA( Vaddr, StartBitno, MaskBits )\
		( ((((ULONG*)(Vaddr))[(StartBitno)>>5])>>((StartBitno)&0x1F)) & (MaskBits) )
/*--------------------------------------------------------------------------------------------------*/
#define	SET_BITSDATA( SetAddr, Vaddr, StartBitno, SetBits, MaskBits )\
		(((ULONG*)(SetAddr))[(StartBitno)>>5]) = ( (((SetBits)&(MaskBits))<<((StartBitno)&0x1F)) |\
			(((((ULONG*)(Vaddr))[(StartBitno)>>5])) & (~((MaskBits)<<((StartBitno)&0x1F)))) )
/*--------------------------------------------------------------------------------------------------*/
/*		Repeat Macro Definition																		*/
/*--------------------------------------------------------------------------------------------------*/
#define REPEAT02( x )					(x),(x)
#define REPEAT03( x )					(x),(x),(x)
#define REPEAT04( x )					(x),(x),(x),(x)
#define REPEAT05( x )					(x),(x),(x),(x),(x)
#define REPEAT06( x )					(x),(x),(x),(x),(x),(x)
#define REPEAT07( x )					(x),(x),(x),(x),(x),(x),(x)
#define REPEAT08( x )					REPEAT04( (x) ), REPEAT04( (x) )
#define REPEAT12( x )					REPEAT08( (x) ), REPEAT04( (x) )
#define REPEAT16( x )					REPEAT08( (x) ), REPEAT08( (x) )
#define REPEAT32( x )					REPEAT16( (x) ), REPEAT16( (x) )
#define REPEAT48( x )					REPEAT32( (x) ), REPEAT16( (x) )
#define REPEAT64( x )					REPEAT32( (x) ), REPEAT32( (x) )
/*--------------------------------------------------------------------------------------------------*/

/****************************************************************************************************/
/*																									*/
/*			Immediate Binary Data Definition														*/
/*																									*/
/****************************************************************************************************/
#define		Bx0000						0x00
#define		Bx0001						0x01
#define		Bx0010						0x02
#define		Bx0011						0x03
#define		Bx0100						0x04
#define		Bx0101						0x05
#define		Bx0110						0x06
#define		Bx0111						0x07
#define		Bx1000						0x08
#define		Bx1001						0x09
#define		Bx1010						0x0A
#define		Bx1011						0x0B
#define		Bx1100						0x0C
#define		Bx1101						0x0D
#define		Bx1110						0x0E
#define		Bx1111						0x0F
/*--------------------------------------------------------------------------------------------------*/
#define		Bx0000x0000					0x00
#define		Bx0000x0001					0x01
#define		Bx0000x0010					0x02
#define		Bx0000x0011					0x03
#define		Bx0000x0100					0x04
#define		Bx0000x0101					0x05
#define		Bx0000x0110					0x06
#define		Bx0000x0111					0x07
#define		Bx0000x1000					0x08
#define		Bx0000x1001					0x09
#define		Bx0000x1010					0x0A
#define		Bx0000x1011					0x0B
#define		Bx0000x1100					0x0C
#define		Bx0000x1101					0x0D
#define		Bx0000x1110					0x0E
#define		Bx0000x1111					0x0F
/*--------------------------------------------------------------------------------------------------*/
#define		Bx0001x0000			0x10
#define		Bx0001x0001			0x11
#define		Bx0001x0010			0x12
#define		Bx0001x0011			0x13
#define		Bx0001x0100			0x14
#define		Bx0001x0101			0x15
#define		Bx0001x0110			0x16
#define		Bx0001x0111			0x17
#define		Bx0001x1000			0x18
#define		Bx0001x1001			0x19
#define		Bx0001x1010			0x1A
#define		Bx0001x1011			0x1B
#define		Bx0001x1100			0x1C
#define		Bx0001x1101			0x1D
#define		Bx0001x1110			0x1E
#define		Bx0001x1111			0x1F
/*--------------------------------------------------------------------------------------------------*/
#define		Bx0010x0000			0x20
#define		Bx0010x0001			0x21
#define		Bx0010x0010			0x22
#define		Bx0010x0011			0x23
#define		Bx0010x0100			0x24
#define		Bx0010x0101			0x25
#define		Bx0010x0110			0x26
#define		Bx0010x0111			0x27
#define		Bx0010x1000			0x28
#define		Bx0010x1001			0x29
#define		Bx0010x1010			0x2A
#define		Bx0010x1011			0x2B
#define		Bx0010x1100			0x2C
#define		Bx0010x1101			0x2D
#define		Bx0010x1110			0x2E
#define		Bx0010x1111			0x2F
/*--------------------------------------------------------------------------------------------------*/
#define		Bx0011x0000			0x30
#define		Bx0011x0001			0x31
#define		Bx0011x0010			0x32
#define		Bx0011x0011			0x33
#define		Bx0011x0100			0x34
#define		Bx0011x0101			0x35
#define		Bx0011x0110			0x36
#define		Bx0011x0111			0x37
#define		Bx0011x1000			0x38
#define		Bx0011x1001			0x39
#define		Bx0011x1010			0x3A
#define		Bx0011x1011			0x3B
#define		Bx0011x1100			0x3C
#define		Bx0011x1101			0x3D
#define		Bx0011x1110			0x3E
#define		Bx0011x1111			0x3F
/*--------------------------------------------------------------------------------------------------*/
#define		Bx0100x0000			0x40
#define		Bx0100x0001			0x41
#define		Bx0100x0010			0x42
#define		Bx0100x0011			0x43
#define		Bx0100x0100			0x44
#define		Bx0100x0101			0x45
#define		Bx0100x0110			0x46
#define		Bx0100x0111			0x47
#define		Bx0100x1000			0x48
#define		Bx0100x1001			0x49
#define		Bx0100x1010			0x4A
#define		Bx0100x1011			0x4B
#define		Bx0100x1100			0x4C
#define		Bx0100x1101			0x4D
#define		Bx0100x1110			0x4E
#define		Bx0100x1111			0x4F
/*--------------------------------------------------------------------------------------------------*/
#define		Bx0101x0000			0x50
#define		Bx0101x0001			0x51
#define		Bx0101x0010			0x52
#define		Bx0101x0011			0x53
#define		Bx0101x0100			0x54
#define		Bx0101x0101			0x55
#define		Bx0101x0110			0x56
#define		Bx0101x0111			0x57
#define		Bx0101x1000			0x58
#define		Bx0101x1001			0x59
#define		Bx0101x1010			0x5A
#define		Bx0101x1011			0x5B
#define		Bx0101x1100			0x5C
#define		Bx0101x1101			0x5D
#define		Bx0101x1110			0x5E
#define		Bx0101x1111			0x5F
/*--------------------------------------------------------------------------------------------------*/
#define		Bx0110x0000			0x60
#define		Bx0110x0001			0x61
#define		Bx0110x0010			0x62
#define		Bx0110x0011			0x63
#define		Bx0110x0100			0x64
#define		Bx0110x0101			0x65
#define		Bx0110x0110			0x66
#define		Bx0110x0111			0x67
#define		Bx0110x1000			0x68
#define		Bx0110x1001			0x69
#define		Bx0110x1010			0x6A
#define		Bx0110x1011			0x6B
#define		Bx0110x1100			0x6C
#define		Bx0110x1101			0x6D
#define		Bx0110x1110			0x6E
#define		Bx0110x1111			0x6F
/*--------------------------------------------------------------------------------------------------*/
#define		Bx0111x0000			0x70
#define		Bx0111x0001			0x71
#define		Bx0111x0010			0x72
#define		Bx0111x0011			0x73
#define		Bx0111x0100			0x74
#define		Bx0111x0101			0x75
#define		Bx0111x0110			0x76
#define		Bx0111x0111			0x77
#define		Bx0111x1000			0x78
#define		Bx0111x1001			0x79
#define		Bx0111x1010			0x7A
#define		Bx0111x1011			0x7B
#define		Bx0111x1100			0x7C
#define		Bx0111x1101			0x7D
#define		Bx0111x1110			0x7E
#define		Bx0111x1111			0x7F
/*--------------------------------------------------------------------------------------------------*/
#define		Bx1000x0000			0x80
#define		Bx1000x0001			0x81
#define		Bx1000x0010			0x82
#define		Bx1000x0011			0x83
#define		Bx1000x0100			0x84
#define		Bx1000x0101			0x85
#define		Bx1000x0110			0x86
#define		Bx1000x0111			0x87
#define		Bx1000x1000			0x88
#define		Bx1000x1001			0x89
#define		Bx1000x1010			0x8A
#define		Bx1000x1011			0x8B
#define		Bx1000x1100			0x8C
#define		Bx1000x1101			0x8D
#define		Bx1000x1110			0x8E
#define		Bx1000x1111			0x8F
/*--------------------------------------------------------------------------------------------------*/
#define		Bx1001x0000			0x90
#define		Bx1001x0001			0x91
#define		Bx1001x0010			0x92
#define		Bx1001x0011			0x93
#define		Bx1001x0100			0x94
#define		Bx1001x0101			0x95
#define		Bx1001x0110			0x96
#define		Bx1001x0111			0x97
#define		Bx1001x1000			0x98
#define		Bx1001x1001			0x99
#define		Bx1001x1010			0x9A
#define		Bx1001x1011			0x9B
#define		Bx1001x1100			0x9C
#define		Bx1001x1101			0x9D
#define		Bx1001x1110			0x9E
#define		Bx1001x1111			0x9F
/*--------------------------------------------------------------------------------------------------*/
#define		Bx1010x0000			0xA0
#define		Bx1010x0001			0xA1
#define		Bx1010x0010			0xA2
#define		Bx1010x0011			0xA3
#define		Bx1010x0100			0xA4
#define		Bx1010x0101			0xA5
#define		Bx1010x0110			0xA6
#define		Bx1010x0111			0xA7
#define		Bx1010x1000			0xA8
#define		Bx1010x1001			0xA9
#define		Bx1010x1010			0xAA
#define		Bx1010x1011			0xAB
#define		Bx1010x1100			0xAC
#define		Bx1010x1101			0xAD
#define		Bx1010x1110			0xAE
#define		Bx1010x1111			0xAF
/*--------------------------------------------------------------------------------------------------*/
#define		Bx1011x0000			0xB0
#define		Bx1011x0001			0xB1
#define		Bx1011x0010			0xB2
#define		Bx1011x0011			0xB3
#define		Bx1011x0100			0xB4
#define		Bx1011x0101			0xB5
#define		Bx1011x0110			0xB6
#define		Bx1011x0111			0xB7
#define		Bx1011x1000			0xB8
#define		Bx1011x1001			0xB9
#define		Bx1011x1010			0xBA
#define		Bx1011x1011			0xBB
#define		Bx1011x1100			0xBC
#define		Bx1011x1101			0xBD
#define		Bx1011x1110			0xBE
#define		Bx1011x1111			0xBF
/*--------------------------------------------------------------------------------------------------*/
#define		Bx1100x0000			0xC0
#define		Bx1100x0001			0xC1
#define		Bx1100x0010			0xC2
#define		Bx1100x0011			0xC3
#define		Bx1100x0100			0xC4
#define		Bx1100x0101			0xC5
#define		Bx1100x0110			0xC6
#define		Bx1100x0111			0xC7
#define		Bx1100x1000			0xC8
#define		Bx1100x1001			0xC9
#define		Bx1100x1010			0xCA
#define		Bx1100x1011			0xCB
#define		Bx1100x1100			0xCC
#define		Bx1100x1101			0xCD
#define		Bx1100x1110			0xCE
#define		Bx1100x1111			0xCF
/*--------------------------------------------------------------------------------------------------*/
#define		Bx1101x0000			0xD0
#define		Bx1101x0001			0xD1
#define		Bx1101x0010			0xD2
#define		Bx1101x0011			0xD3
#define		Bx1101x0100			0xD4
#define		Bx1101x0101			0xD5
#define		Bx1101x0110			0xD6
#define		Bx1101x0111			0xD7
#define		Bx1101x1000			0xD8
#define		Bx1101x1001			0xD9
#define		Bx1101x1010			0xDA
#define		Bx1101x1011			0xDB
#define		Bx1101x1100			0xDC
#define		Bx1101x1101			0xDD
#define		Bx1101x1110			0xDE
#define		Bx1101x1111			0xDF
/*--------------------------------------------------------------------------------------------------*/
#define		Bx1110x0000			0xE0
#define		Bx1110x0001			0xE1
#define		Bx1110x0010			0xE2
#define		Bx1110x0011			0xE3
#define		Bx1110x0100			0xE4
#define		Bx1110x0101			0xE5
#define		Bx1110x0110			0xE6
#define		Bx1110x0111			0xE7
#define		Bx1110x1000			0xE8
#define		Bx1110x1001			0xE9
#define		Bx1110x1010			0xEA
#define		Bx1110x1011			0xEB
#define		Bx1110x1100			0xEC
#define		Bx1110x1101			0xED
#define		Bx1110x1110			0xEE
#define		Bx1110x1111			0xEF
/*--------------------------------------------------------------------------------------------------*/
#define		Bx1111x0000			0xF0
#define		Bx1111x0001			0xF1
#define		Bx1111x0010			0xF2
#define		Bx1111x0011			0xF3
#define		Bx1111x0100			0xF4
#define		Bx1111x0101			0xF5
#define		Bx1111x0110			0xF6
#define		Bx1111x0111			0xF7
#define		Bx1111x1000			0xF8
#define		Bx1111x1001			0xF9
#define		Bx1111x1010			0xFA
#define		Bx1111x1011			0xFB
#define		Bx1111x1100			0xFC
#define		Bx1111x1101			0xFD
#define		Bx1111x1110			0xFE
#define		Bx1111x1111			0xFF
/*--------------------------------------------------------------------------------------------------*/

#endif
