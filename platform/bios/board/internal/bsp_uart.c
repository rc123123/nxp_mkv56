#include "bsp_uart.h"

static void UART0_InitPins();

static void UART0_InitPins()
{
    /* Port B Clock Gate Control: Clock enabled */
    CLOCK_EnableClock(kCLOCK_PortB);
    
    /* PORTB0 (pin 53) is configured as UART0_RX */
    PORT_SetPinMux(UART0_RX_PORT, UART0_RX_PIN, kPORT_MuxAlt7);
    
    /* PORTB1 (pin 54) is configured as UART0_TX */
    PORT_SetPinMux(UART0_TX_PORT, UART0_TX_PIN, kPORT_MuxAlt7);
    
    SIM->SOPT5 = ((SIM->SOPT5 &
                   /* Mask bits to zero which are setting */
                   (~(SIM_SOPT5_UART0TXSRC_MASK | SIM_SOPT5_UART0RXSRC_MASK)))
                  
                  /* UART 0 transmit data source select: UART0_TX pin. */
                  | SIM_SOPT5_UART0TXSRC(SOPT5_UART0_TX_SRC)
                      
                      /* UART 0 receive data source select: UART0_RX pin. */
                      | SIM_SOPT5_UART0RXSRC(SOPT5_UART0_RX_SRC));
    
    return;
}

//static void UART0_DMA_InitClock()
//{
//    /* Global initialization */
//    DMAMUX_Init(DMA_DMAMUX_BASEADDR);
//    //EDMA_Init(DMA_DMA_BASEADDR, &DMA_config);
//    
//    return;
//}

void UART0_DMA_Init()
{
    uart_config_t UART0_config;

    /* 1 引脚复用 */
    UART0_InitPins();
    
    UART_GetDefaultConfig(&UART0_config);
    
    UART0_config.baudRate_Bps = 256000;
	
    UART_Init(UART0, &UART0_config, UART0_CLOCK_SOURCE);
    
    UART_EnableInterrupts(UART0, kUART_IdleLineInterruptEnable);
    UART_EnableInterrupts(UART0,kUART_RxOverrunInterruptEnable);
    UART_EnableInterrupts(UART0,kUART_FramingErrorInterruptEnable);
    UART_EnableInterrupts(UART0,kUART_RxDataRegFullInterruptEnable);
	EnableIRQ(UART4_RX_TX_IRQn);
    EnableIRQ(UART4_ERR_IRQn);
    
}


void UART0_Init(void)
{
    uart_config_t UART0_config;
    
    /* 1 引脚复用 */
    UART0_InitPins();
    
    UART_GetDefaultConfig(&UART0_config);
    
    UART0_config.baudRate_Bps = 256000;
    UART0_config.enableRx = true;
    UART0_config.enableTx = true;
	
    UART_Init(UART0, &UART0_config, UART0_CLOCK_SOURCE);
    
    UART_EnableInterrupts(UART0, kUART_IdleLineInterruptEnable);
    UART_EnableInterrupts(UART0,kUART_RxOverrunInterruptEnable);
    UART_EnableInterrupts(UART0,kUART_FramingErrorInterruptEnable);
    UART_EnableInterrupts(UART0,kUART_RxDataRegFullInterruptEnable);
	EnableIRQ(UART0_RX_TX_IRQn);
    EnableIRQ(UART0_ERR_IRQn);
}