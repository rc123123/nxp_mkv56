#include "bsp_i2c.h"



i2c_master_handle_t g_i2c1MasterHandle;

i2c_master_edma_handle_t tmp175_m_dma_handle;
edma_handle_t tmp175_edmaHandle;

volatile bool completionFlag = false;
volatile bool nakFlag        = false;

volatile bool tmp175_MasterCompletionFlag = false; 

uint32_t g_enterI2C1DMAInterruptCount = 0;

static void I2C1_MasterCallback(I2C_Type *base, i2c_master_handle_t *handle, status_t status, void *userData);
static void I2C_ReleaseBusDelay(void);
static void I2C1_ReleaseBus();

static void I2C1_MasterCallback(I2C_Type *base, i2c_master_handle_t *handle, status_t status, void *userData)
{
    /* Signal transfer success when received success status. */
    if (status == kStatus_Success)
    {
        completionFlag = true;
    }
    /* Signal transfer success when received success status. */
    if ((status == kStatus_I2C_Nak) || (status == kStatus_I2C_Addr_Nak))
    {
        nakFlag = true;
    }
}

static void Tmp175_i2c1_master_callback(I2C_Type *base, i2c_master_edma_handle_t *handle, status_t status, void *userData)
{
    /* Signal transfer success when received success status. */
    if (status == kStatus_Success)
    {
        tmp175_MasterCompletionFlag = true;
    }

    g_enterI2C1DMAInterruptCount++;
}

static void I2C_ReleaseBusDelay(void)
{
    uint32_t i = 0;
    for (i = 0; i < I2C_RELEASE_BUS_COUNT; i++)
    {
        __NOP();  //??
    }

    return;
}

static void I2C1_ReleaseBus()
{
	uint8_t i = 0;
    gpio_pin_config_t pin_config;
    port_pin_config_t i2c_pin_config = {0};

    CLOCK_EnableClock(kCLOCK_PortC);
    /* Config pin mux as gpio */
    i2c_pin_config.pullSelect = kPORT_PullUp;
    i2c_pin_config.mux        = kPORT_MuxAsGpio;

    pin_config.pinDirection = kGPIO_DigitalOutput;
    pin_config.outputLogic  = 1U;
    PORT_SetPinConfig(I2C1_SCL_PORT, I2C1_SCL_PIN, &i2c_pin_config);
    PORT_SetPinConfig(I2C1_SDA_PORT, I2C1_SDA_PIN, &i2c_pin_config);

    GPIO_PinInit(I2C1_SCL_GPIO, I2C1_SCL_PIN, &pin_config);
    GPIO_PinInit(I2C1_SDA_GPIO, I2C1_SDA_PIN, &pin_config);

    /* Drive SDA low first to simulate a start */
    GPIO_PinWrite(I2C1_SDA_GPIO, I2C1_SDA_PIN, 0U);
    I2C_ReleaseBusDelay();


    /* Send 9 pulses on SCL and keep SDA high */
    for (i = 0; i < 9; i++)
    {
        GPIO_PinWrite(I2C1_SCL_GPIO, I2C1_SCL_PIN, 0U);
        I2C_ReleaseBusDelay();

        GPIO_PinWrite(I2C1_SDA_GPIO, I2C1_SDA_PIN, 1U);
        I2C_ReleaseBusDelay();

        GPIO_PinWrite(I2C1_SCL_GPIO, I2C1_SCL_PIN, 1U);
        I2C_ReleaseBusDelay();
        I2C_ReleaseBusDelay();
    }

    /* Send stop */
    GPIO_PinWrite(I2C1_SCL_GPIO, I2C1_SCL_PIN, 0U);
    I2C_ReleaseBusDelay();

    GPIO_PinWrite(I2C1_SDA_GPIO, I2C1_SDA_PIN, 0U);
    I2C_ReleaseBusDelay();

    GPIO_PinWrite(I2C1_SCL_GPIO, I2C1_SCL_PIN, 1U);
    I2C_ReleaseBusDelay();

    GPIO_PinWrite(I2C1_SDA_GPIO, I2C1_SDA_PIN, 1U);
    I2C_ReleaseBusDelay();
}

void I2C1_Delay()
{
	DelayUs(10);
    
    return;
}

void I2C1_Start()
{
	I2C1_SCL(1);
	I2C1_SDA(1);
	I2C1_Delay();
	I2C1_SDA(0);
	I2C1_Delay();
	I2C1_SCL(0);
	I2C1_Delay();
    
    return;
}

void I2C1_Stop()
{
	I2C1_SDA(0);
	I2C1_Delay();
	I2C1_SCL(1);
	I2C1_Delay();
	I2C1_SDA(1);
	I2C1_Delay();
    
    return;
}

xint8u I2C1_WaitAck()
{
    xint8u waittime = 0;
	xint8u rack = 0;
    
    
	I2C1_SDA(1); 			/* 主机释放 SDA 线(此时外部器件可以拉低 SDA 线) */
    
    //I2C1_SDA_IN;
	I2C1_Delay();
	I2C1_SCL(1); /* SCL=1, 此时从机可以返回 ACK */
	I2C1_Delay();

	while (I2C1_READ_PIN) /* 等待应答 */
	{
		waittime++;
		if(waittime > 250)
		{
			I2C1_Stop();
			rack = 1;
			break;
		}
	}

	I2C1_SCL(0); /* SCL=0, 结束 ACK 检查 */
	I2C1_Delay();	
    
	return rack;
}

void I2C1_Ack()
{
	I2C1_SDA(0); /* SCL 0 -> 1 时 SDA = 0,表示应答 */
	I2C1_Delay();
	I2C1_SCL(1); /* 产生一个时钟 */
	I2C1_Delay();
	I2C1_SCL(0);
	I2C1_Delay();
	I2C1_SDA(1); /* 主机释放 SDA 线 */
	I2C1_Delay();

	return;
}

void I2C1_Nack()
{
	I2C1_SDA(1); /* SCL 0 -> 1 时 SDA = 1,表示不应答 */
	I2C1_Delay();
	I2C1_SCL(1); /* 产生一个时钟 */
	I2C1_Delay();
	I2C1_SCL(0);
	I2C1_Delay();

	return;
}

void I2C1_SoftwareInit(void)
{
	I2C1_ReleaseBus();

	/* Port D Clock Gate Control: Clock enabled */
    CLOCK_EnableClock(kCLOCK_PortC);

    

	const gpio_pin_config_t gpioc10_pin82_config = {kGPIO_DigitalOutput, 1};
	GPIO_PinInit(GPIOC, 10U, &gpioc10_pin82_config);

	const gpio_pin_config_t gpioc11_pin83_config = {kGPIO_DigitalOutput, 1};
	GPIO_PinInit(GPIOC, 11U, &gpioc11_pin83_config);

    //port滤波
//    /* Configure digital filter */
//    PORT_EnablePinsDigitalFilter(
//        /* Digital filter is configured on port D */
//        PORTC,
//        /* Digital filter is configured for PORTD0 */
//        PORT_DFER_DFE_10_MASK
//            /* Digital filter is configured for PORTD1 */
//            | PORT_DFER_DFE_11_MASK,
//        /* Disable digital filter */
//        false);

	PORT_SetPinMux(I2C1_SCL_PORT, I2C1_SCL_PIN, kPORT_MuxAsGpio);
	PORT_SetPinMux(I2C1_SDA_PORT, I2C1_SDA_PIN, kPORT_MuxAsGpio);

    const port_pin_config_t portc10_pin82_config = {/* Internal pull-up resistor is enabled */
                                                    kPORT_PullUp,
                                                    /* Fast slew rate is configured */
                                                    kPORT_FastSlewRate,
                                                    /* Passive filter is disabled */
                                                    kPORT_PassiveFilterDisable,
                                                    /* Open drain is enabled */
                                                    kPORT_OpenDrainEnable,
                                                    /* Low drive strength is configured */
                                                    kPORT_LowDriveStrength,
                                                    /* Pin is configured as I2C1_SCL */
                                                    kPORT_MuxAsGpio,
                                                    /* Pin Control Register fields [15:0] are not locked */
                                                    kPORT_UnlockRegister};
    /* PORTD8 (pin 137) is configured as I2C1_SCL */
    PORT_SetPinConfig(PORTC, 10U, &portc10_pin82_config);

    const port_pin_config_t portc11_pin83_config = {/* Internal pull-up resistor is enabled */
                                                    kPORT_PullUp,
                                                    /* Fast slew rate is configured */
                                                    kPORT_FastSlewRate,
                                                    /* Passive filter is disabled */
                                                    kPORT_PassiveFilterDisable,
                                                    /* Open drain is enabled */
                                                    kPORT_OpenDrainEnable,
                                                    /* Low drive strength is configured */
                                                    kPORT_LowDriveStrength,
                                                    /* Pin is configured as I2C1_SDA */
                                                    kPORT_MuxAsGpio,
                                                    /* Pin Control Register fields [15:0] are not locked */
                                                    kPORT_UnlockRegister};
	PORT_SetPinConfig(PORTC, 11U, &portc11_pin83_config);

	return;
}

void I2C1_WriteByte(xint8u data)
{
    xint8u bitNunber;
    for (bitNunber = 0; bitNunber < 8; bitNunber++)
    {
        I2C1_SDA((data & 0x80) >> 7); /* 高位先发送 */
        I2C1_Delay();
        I2C1_SCL(1);
        I2C1_Delay();
        I2C1_SCL(0);
        //I2C1_Delay();
        data <<= 1; /* 左移 1 位,用于下一次发送 */
    }
    I2C1_SDA(1); /* 发送完成, 主机释放 SDA 线 */
}

xint8u I2C1_ReadByte(xint8u ack)
{
    xint8u i, receiveData = 0;
    
    //I2C1_SDA_IN;
    for (i = 0; i < 8; i++ ) /* 接收 1 个字节数据 */
    {
        receiveData <<= 1; /* 高位先输出,所以先收到的数据位要左移 */
        I2C1_SCL(1);
        I2C1_Delay();
        if (I2C1_READ_PIN)
        {
            receiveData++;
        }
        I2C1_SCL(0);
        I2C1_Delay();
    }
    //I2C1_SDA_OUT;
    if (!ack)
    {
        I2C1_Nack(); /* 发送 nACK */
    }
    else
    {
        I2C1_Ack(); /* 发送 ACK */
    }
    return receiveData;
}


void I2C1_HardwareInit(void)
{
	/* Port D Clock Gate Control: Clock enabled */
    CLOCK_EnableClock(kCLOCK_PortC);

    //port滤波
//    /* Configure digital filter */
//    PORT_EnablePinsDigitalFilter(
//        /* Digital filter is configured on port D */
//        PORTC,
//        /* Digital filter is configured for PORTD0 */
//        PORT_DFER_DFE_10_MASK
//            /* Digital filter is configured for PORTD1 */
//            | PORT_DFER_DFE_11_MASK,
//        /* Disable digital filter */
//        false);

    const port_pin_config_t portc10_pin82_config = {/* Internal pull-up resistor is enabled */
                                                    kPORT_PullUp,
                                                    /* Fast slew rate is configured */
                                                    kPORT_FastSlewRate,
                                                    /* Passive filter is disabled */
                                                    kPORT_PassiveFilterDisable,
                                                    /* Open drain is enabled */
                                                    kPORT_OpenDrainEnable,
                                                    /* Low drive strength is configured */
                                                    kPORT_LowDriveStrength,
                                                    /* Pin is configured as I2C1_SCL */
                                                    kPORT_MuxAlt2,
                                                    /* Pin Control Register fields [15:0] are not locked */
                                                    kPORT_UnlockRegister};
    /* PORTD8 (pin 137) is configured as I2C1_SCL */
    PORT_SetPinConfig(PORTC, 10U, &portc10_pin82_config);

    const port_pin_config_t portc11_pin83_config = {/* Internal pull-up resistor is enabled */
                                                    kPORT_PullUp,
                                                    /* Fast slew rate is configured */
                                                    kPORT_FastSlewRate,
                                                    /* Passive filter is disabled */
                                                    kPORT_PassiveFilterDisable,
                                                    /* Open drain is enabled */
                                                    kPORT_OpenDrainEnable,
                                                    /* Low drive strength is configured */
                                                    kPORT_LowDriveStrength,
                                                    /* Pin is configured as I2C1_SDA */
                                                    kPORT_MuxAlt2,
                                                    /* Pin Control Register fields [15:0] are not locked */
                                                    kPORT_UnlockRegister};
	PORT_SetPinConfig(PORTC, 11U, &portc11_pin83_config);
    
    i2c_master_config_t masterConfig;
        
    I2C_MasterTransferCreateHandle(I2C1, &g_i2c1MasterHandle, I2C1_MasterCallback, NULL);

    I2C_MasterGetDefaultConfig(&masterConfig);

    masterConfig.baudRate_Bps = I2C1_BAUDRATE;

    I2C_MasterInit(I2C1, &masterConfig, CLOCK_GetFreq(I2C1_CLK_SRC));
}

xbool I2C_WriteData(I2C_Type *base, uint8_t device_addr, uint8_t reg_addr, uint8_t value)
{
	i2c_master_transfer_t masterXfer;
    memset(&masterXfer, 0, sizeof(masterXfer));

    masterXfer.slaveAddress   = device_addr;
    masterXfer.direction      = kI2C_Write;
    masterXfer.subaddress     = reg_addr;
    masterXfer.subaddressSize = 2;
    masterXfer.data           = &value;
    masterXfer.dataSize       = 1;
    masterXfer.flags          = kI2C_TransferDefaultFlag;

    /*  direction=write : start+device_write;cmdbuff;xBuff; */
    /*  direction=recive : start+device_write;cmdbuff;repeatStart+device_read;xBuff; */

    I2C_MasterTransferNonBlocking(base, &g_i2c1MasterHandle, &masterXfer);

    /*  wait for transfer completed. */
    while ((!nakFlag) && (!completionFlag))
    {
    }

    nakFlag = false;

    if (completionFlag == true)
    {
        completionFlag = false;
        return true;
    }
    else
    {
        return false;
    }
}

xbool I2C_ReadData(I2C_Type *base, uint8_t device_addr, uint8_t reg_addr, uint8_t *rxBuff, uint32_t rxSize)
{
	i2c_master_transfer_t masterXfer;
    memset(&masterXfer, 0, sizeof(masterXfer));
    
    masterXfer.slaveAddress   = device_addr;
    masterXfer.direction      = kI2C_Read;
    masterXfer.subaddress     = reg_addr;
    masterXfer.subaddressSize = 1;
    masterXfer.data           = rxBuff;
    masterXfer.dataSize       = rxSize;
    masterXfer.flags          = kI2C_TransferDefaultFlag;

    /*  direction=write : start+device_write;cmdbuff;xBuff; */
    /*  direction=recive : start+device_write;cmdbuff;repeatStart+device_read;xBuff; */

    I2C_MasterTransferNonBlocking(base, &g_i2c1MasterHandle, &masterXfer);

    /*  wait for transfer completed. */
    while ((!nakFlag) && (!completionFlag))
    {
    }

    nakFlag = false;

    if (completionFlag == true)
    {
        completionFlag = false;
        return true;
    }
    else
    {
        return false;
    }
}

void I2C1_DMAModeInit()
{
	/* Port D Clock Gate Control: Clock enabled */
    CLOCK_EnableClock(kCLOCK_PortC);

    /*--------------------------------------------------------------------------------------------------*/
	/*	pin config																						*/
	/*--------------------------------------------------------------------------------------------------*/
	/*I2C1*/
    const port_pin_config_t portc10_pin82_config = {/* Internal pull-up resistor is enabled */
                                                    kPORT_PullUp,
                                                    /* Fast slew rate is configured */
                                                    kPORT_FastSlewRate,
                                                    /* Passive filter is disabled */
                                                    kPORT_PassiveFilterDisable,
                                                    /* Open drain is enabled */
                                                    kPORT_OpenDrainEnable,
                                                    /* Low drive strength is configured */
                                                    kPORT_LowDriveStrength,
                                                    /* Pin is configured as I2C1_SCL */
                                                    kPORT_MuxAlt2,
                                                    /* Pin Control Register fields [15:0] are not locked */
                                                    kPORT_UnlockRegister};
    /* PORTC10 (pin 82) is configured as I2C1_SCL */
    PORT_SetPinConfig(PORTC, 10U, &portc10_pin82_config);

	const port_pin_config_t portc11_pin83_config = {/* Internal pull-up resistor is enabled */
                                                    kPORT_PullUp,
                                                    /* Fast slew rate is configured */
                                                    kPORT_FastSlewRate,
                                                    /* Passive filter is disabled */
                                                    kPORT_PassiveFilterDisable,
                                                    /* Open drain is enabled */
                                                    kPORT_OpenDrainEnable,
                                                    /* Low drive strength is configured */
                                                    kPORT_LowDriveStrength,
                                                    /* Pin is configured as I2C1_SDA */
                                                    kPORT_MuxAlt2,
                                                    /* Pin Control Register fields [15:0] are not locked */
                                                    kPORT_UnlockRegister};
    /* PORTC11 (pin 83) is configured as I2C1_SDA */
    PORT_SetPinConfig(PORTC, 11U, &portc11_pin83_config);

    i2c_master_config_t masterConfig;
	/*--------------------------------------------------------------------------------------------------*/
    /*      Config I2C1 as master mode                                                                  */
    /*--------------------------------------------------------------------------------------------------*/
	I2C_MasterGetDefaultConfig(&masterConfig);
	masterConfig.baudRate_Bps = I2C1_BAUDRATE;	
	I2C_MasterInit(I2C1, &masterConfig, CLOCK_GetFreq(I2C1_CLK_SRC));

	//edma_config_t userConfig;
	/*--------------------------------------------------------------------------------------------------*/
	/*      Config I2C1 DMA                                                                             */
	/*--------------------------------------------------------------------------------------------------*/
	/* DMA MUX init */
	//DMAMUX_Init(DMAMUX0);
	DMAMUX_SetSource(DMAMUX0, TMP175_DMA_CHANNEL, kDmaRequestMux0Group1I2C1);
	DMAMUX_EnableChannel(DMAMUX0, TMP175_DMA_CHANNEL);

	/* EDMA init */
	//EDMA_GetDefaultConfig(&userConfig);
	//EDMA_Init(DMA0, &userConfig);
	EDMA_CreateHandle(&tmp175_edmaHandle, DMA0, TMP175_DMA_CHANNEL);
    
    memset(&tmp175_m_dma_handle, 0, sizeof(tmp175_m_dma_handle));
    
	I2C_MasterCreateEDMAHandle(I2C1, &tmp175_m_dma_handle, Tmp175_i2c1_master_callback, NULL, &tmp175_edmaHandle);

	return;
}

xbool I2C_DMAModeWriteData(I2C_Type *base, uint8_t deviceAddr, uint8_t regAddr, uint8_t *txBuff, uint32_t txSize)
{
	i2c_master_transfer_t masterXfer;
	tmp175_MasterCompletionFlag = false;
	uint32_t countNumber = 0;

	memset(&masterXfer, 0, sizeof(masterXfer));

	masterXfer.slaveAddress   = deviceAddr;
    masterXfer.direction      = kI2C_Write;
    masterXfer.subaddress     = (uint32_t)regAddr;
    masterXfer.subaddressSize = 1;
    masterXfer.data           = txBuff;
    masterXfer.dataSize       = txSize;
    masterXfer.flags          = kI2C_TransferDefaultFlag;

    I2C_MasterTransferEDMA(base, &tmp175_m_dma_handle, &masterXfer);
    
	while(!tmp175_MasterCompletionFlag)
    {
        DelayMs(1);
        //OSTimeDly(1);
        countNumber++;
        if(countNumber == 10)
			return false;            
    }

    return true;
}

xbool I2C_DMAModeReadData(I2C_Type *base, uint8_t deviceAddr, uint8_t regAddr, uint8_t *rxBuff, uint32_t rxSize)
{
	i2c_master_transfer_t masterXfer;
	uint32_t countNumber = 0;

	memset(&masterXfer, 0, sizeof(masterXfer));

	tmp175_MasterCompletionFlag = false;
	g_enterI2C1DMAInterruptCount = 0;
    
    masterXfer.slaveAddress   = deviceAddr;
    masterXfer.direction      = kI2C_Read;
    masterXfer.subaddress     = (uint32_t)regAddr;
    masterXfer.subaddressSize = 1;
    masterXfer.data           = rxBuff;
    masterXfer.dataSize       = rxSize;
    masterXfer.flags          = kI2C_TransferDefaultFlag;

    I2C_MasterTransferEDMA(base, &tmp175_m_dma_handle, &masterXfer);
    
    //tmp175_MasterCompletionFlag = false;
    
	while(!tmp175_MasterCompletionFlag)
    {
        DelayMs(1);
        //OSTimeDly(1);
        countNumber++;
        if(countNumber == 10)
			return false;            
    }

    return true;
}