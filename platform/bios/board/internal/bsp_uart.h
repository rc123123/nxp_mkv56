#ifndef _BSP_UART_H_
#define _BSP_UART_H_


//#include "fsl_gpio.h"
#include "fsl_port.h"
#include "fsl_dmamux.h"
#include "fsl_uart.h"


#define UART0_RX_PORT PORTB
#define UART0_RX_PIN 0U
#define UART0_RX_PIN_MASK (1U << 0U)

#define UART0_TX_PORT PORTB
#define UART0_TX_PIN 1U
#define UART0_TX_PIN_MASK (1U << 1U)

#define SOPT5_UART0_RX_SRC 0x00u /*!<@brief UART 0 receive data source select: UART0_RX pin */
#define SOPT5_UART0_TX_SRC 0x00u /*!<@brief UART 0 transmit data source select: UART0_TX pin */

/* Used DMA device. */
#define DMA_DMA_BASEADDR DMA0
/* Associated DMAMUX device that is used for muxing of requests. */
#define DMA_DMAMUX_BASEADDR DMAMUX

#define UART0_CLOCK_SOURCE CLOCK_GetFreq(UART0_CLK_SRC)

#if defined(__cplusplus)
extern "C" {
#endif /*_cplusplus. */
    

void UART0_DMA_Init();
void UART0_Init();


/* @} */
#if defined(__cplusplus)
}
#endif /*_cplusplus. */
/*@}*/

#endif