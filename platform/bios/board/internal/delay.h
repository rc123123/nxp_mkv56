/*********************************************************************
* Copyright  CGXi Intelligent Manufacturing Wuxi Ltd.
* File       delay.h
* Brief 	                                         
* --------------------------------------------------------------------
* | Date	    | Version	| Author     | Description
* | 		    |           |            |
* |             |           |            |
* | 2022-06-30  | 1.0.0		| RuiCong    | Create File
**********************************************************************/

#ifndef DELAY_H
#define DELAY_H

#include "baseDef.h"

void DelayUs(uint32 ms);
void DelayMs(uint32 ms);
void OriginDelayMs(uint32 ms);


#endif /* DELAY_H */

