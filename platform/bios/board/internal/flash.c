#include "flash.h"

extern flash_config_t s_flashDriver;

unsigned char ProgramFlash(unsigned char* Src, unsigned int Dest, unsigned short Bytes)
{
	uint8		Success = TRUE;
	int32		FlashOpCode;
    uint32      DestAddr = (uint32)Dest;
    
	DisableInterrupts;  
	BOARD_BootClockRUN();	
	SCB_InvalidateDCache();

	FlashOpCode = FLASH_Program(&s_flashDriver, DestAddr, Src, Bytes); 
	if(FlashOpCode != kStatus_FTFx_Success)
	{
		Success = FALSE;
	}    
    
	SCB_EnableDCache();
	BOARD_BootClockHSRUN();    
	EnableInterrupts;
	
	return Success;
}

//#pragma pack(4) 
//uint8_t flashEraseBuf[] = {
//    0x81, 0xb0, 0x00, 0x21, 0xad, 0xf8, 0x00, 0x10, 
//    0x44, 0xf2, 0x02, 0x01, 0xad, 0xf8, 0x02, 0x10, 
//    0x00, 0x99, 0xc9, 0x1d, 0x09, 0x22, 0x0a, 0x70, 
//    0x49, 0x1e, 0x82, 0x78, 0x0a, 0x70, 0x49, 0x1e, 
//    0x42, 0x78, 0x0a, 0x70, 0x49, 0x1f, 0x80, 0x22, 
//    0x0a, 0x60, 0x0a, 0x78, 0x12, 0xf0, 0x60, 0x0f, 
//    0x01, 0xd0, 0x60, 0x22, 0x0a, 0x60, 0x0a, 0x78, 
//    0x12, 0x06, 0xfc, 0xd5, 0x01, 0xb0, 0x70, 0x47
//};
char FlashDrvCode[1024];
typedef uint32_t (*boot_flash_erase_handler)(uint8_t *Array);
void ProgramFlashErase(unsigned int Dest)
{
	//int32		FlashOpCode;
    uint32      DestAddr = (uint32)Dest;
    uint32      SectorAddr;
    
    uint8_t address_buffer[4];
    uint32_t BootEraseAddr = (uint32_t)FlashDrvCode;        
    boot_flash_erase_handler BootFlashErase = (boot_flash_erase_handler)(BootEraseAddr + 1);
    
	DisableInterrupts;
	BOARD_BootClockRUN();	
	SCB_InvalidateDCache();

    SectorAddr = (DestAddr / FLASH_SECTOR_BYTES) * FLASH_SECTOR_BYTES;
    
    address_buffer[0] = SectorAddr & 0xff;
    address_buffer[1] = (SectorAddr >> 8) & 0xff;
    address_buffer[2] = (SectorAddr >> 16) & 0xff;
    address_buffer[3] = (SectorAddr >> 24) & 0xff;
    BootFlashErase(address_buffer);
  
//	FlashOpCode = FLASH_Erase(&s_flashDriver, (uint32)SectorAddr, FLASH_SECTOR_BYTES, kFLASH_ApiEraseKey);
//	if(FlashOpCode != kStatus_FTFx_Success)
//	{
//	}

	SCB_EnableDCache();
	BOARD_BootClockHSRUN(); 
	EnableInterrupts;    
}

//unsigned int dts1;
//unsigned int dte1;
//unsigned int dt1;
unsigned char ProgramFlashSector(unsigned char* Src, unsigned int Dest)
{
	uint8		Success = TRUE;
	int32		FlashOpCode;
    uint32      DestAddr = (uint32)Dest;
    uint32      SectorAddr;
    
	DisableInterrupts;
	BOARD_BootClockRUN();	
	/*	Clean the D-Cache before reading the flash data. Cache operation is only valid in RUN clock mode. */
	SCB_InvalidateDCache();

    SectorAddr = (DestAddr / FLASH_SECTOR_BYTES) * FLASH_SECTOR_BYTES;
//    /* Set buffer data all as 0xFF*/
//    memset((void*)&M2[0], (int32)0xFFFFFFFF, FLASH_SECTOR_BYTES);
//    /* Copy data */
//    memcpy(&M2[DestAddr-SectorAddr], Src, Bytes);

	/* Erase, program */
//    dts1 = TEST_GetTick();
	FlashOpCode = FLASH_Erase(&s_flashDriver, (uint32)SectorAddr, FLASH_SECTOR_BYTES, kFLASH_ApiEraseKey);
//    dte1 = TEST_GetTick();
//    dt1 = (unsigned int)((dte1 - dts1)/24);
	if(FlashOpCode != kStatus_FTFx_Success)
	{
		/* Alarm */
		Success = FALSE;
	}

	FlashOpCode = FLASH_Program(&s_flashDriver, (uint32)SectorAddr, Src, FLASH_SECTOR_BYTES); 
	if(FlashOpCode != kStatus_FTFx_Success)
	{
		/* Alarm */
		Success = FALSE;
	}    
	
	/* STEP4: Verify */
	//TODO:
	
	/* Clean the D-Cache before reading the flash data. Cache operation is only valid in RUN clock mode. */ 
	SCB_EnableDCache();
	BOARD_BootClockHSRUN();
	EnableInterrupts;
	
	return Success;
}
