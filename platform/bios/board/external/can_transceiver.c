/*********************************************************************
* Copyright  CGXi Intelligent Manufacturing Wuxi Ltd.
* File       can_transceiver.c
* Brief 	                                         
* --------------------------------------------------------------------
* | Date	    | Version	| Author     | Description
* | 		    |           |            |
* |             |           |            |
* | 2022-11-11  | 1.0.0		| RuiCong    | Create File
**********************************************************************/

#include "can_transceiver.h"
#include "delay.h"
#include "board.h"


//static void CANTRANS_NormToSleep(void);





/***********************************************************************
* Name	    CANTRANS_NormToSleep
* Brief     从norm状态进入sleep      
* ----------------------------------------------------------------------
* | 2022-11-11 | RuiCong | Create Function
************************************************************************/
#if defined(ANY_CAN_WAKE)
void CANTRANS_NormToSleep(void)
{
    GPIO_PinWrite(GPIO_CAN_STB, PIN_CAN_STB, 0U);
    DelayMs(1);
}
#endif


/***********************************************************************
* Name	    CANTRANS_GpioInit
* Brief     CAN收发器引脚配置      
* ----------------------------------------------------------------------
* | 2022-11-11 | RuiCong | Create Function
************************************************************************/
void CANTRANS_GpioInit(void)
{
    CLOCK_EnableClock(kCLOCK_PortD);
    
    /* CAN Transfer: EN */
    PORT_SetPinMux(PROT_CAN_EN, PIN_CAN_EN, kPORT_MuxAsGpio);
    gpio_pin_config_t CANTansferENConfig = {kGPIO_DigitalOutput, 1};
    GPIO_PinInit(GPIO_CAN_EN, PIN_CAN_EN, &CANTansferENConfig);
    
    /* CAN Transfer: STBn*/
    PORT_SetPinMux(PROT_CAN_STB, PIN_CAN_STB, kPORT_MuxAsGpio);
    gpio_pin_config_t CANTansferSTBConfig = {kGPIO_DigitalOutput, 1};
    GPIO_PinInit(GPIO_CAN_STB, PIN_CAN_STB, &CANTansferSTBConfig);

	/* CAN Transfer: ERRn */
    PORT_SetPinMux(PROT_CAN_ERR, PIN_CAN_ERR, kPORT_MuxAsGpio);
    gpio_pin_config_t CANTansferERRConfig = {kGPIO_DigitalInput, 0};
    GPIO_PinInit(GPIO_CAN_ERR, PIN_CAN_ERR, &CANTansferERRConfig);

	/* CAN Transfer: KL15 */
    PORT_SetPinMux(PORT_CAN_KL15, PIN_CAN_KL15, kPORT_MuxAsGpio);
    gpio_pin_config_t CANTansferKL15Config = {kGPIO_DigitalInput, 0};
    GPIO_PinInit(GPIO_CAN_KL15, PIN_CAN_KL15, &CANTansferKL15Config);
    
    DelayMs(1);
}




/***********************************************************************
* Name	    CANTRANS_CyclicDetect
* Brief     循环检测是否需要休眠      
* ----------------------------------------------------------------------
* | 2022-11-11 | RuiCong | Create Function
************************************************************************/
void CANTRANS_CyclicDetect(void)
{   
	unsigned char KL15Status = GPIO_PinRead(GPIO_CAN_KL15, PIN_CAN_KL15);

	if(KL15Status)
    {   
        
#if defined(ANY_CAN_WAKE)
        CANTRANS_NormToSleep();
#elif defined(NO_CAN_WAKE)
        BOARD_KL15Sleep();
#endif

    }
}


