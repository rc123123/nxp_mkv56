/*********************************************************************
* Copyright  CGXi Intelligent Manufacturing Wuxi Ltd.
* File       board.c
* Brief 	                                         
* --------------------------------------------------------------------
* | Date	    | Version	| Author     | Description
* | 		    |           |            |
* |             |           |            |
* | 2022-06-30  | 1.0.0		| RuiCong    | Create File
**********************************************************************/



/**************************************** Standard library header file *************************************/
//#include <xxx.h>


/**************************************** Non-standard library header file *********************************/
#include "board.h" 

__no_init BOOT_HOLD_DATA HoldData @".hold_data_ram";

/**************************************** Macros used inside file ******************************************/
//for mcu clock configuration
#define MCG_PLL_DISABLE                                   0U  /*!< MCGPLLCLK disabled				*/
#define OSC_CAP0P                                         0U  /*!< Oscillator 0pF capacitor load	*/
#define SIM_OSC32KSEL_LPO_CLK                             3U  /*!< OSC32KSEL select: LPO clock		*/
#define SIM_PLLFLLSEL_MCGFLLCLK_CLK                       0U  /*!< PLLFLL select: MCGFLLCLK clock	*/
#define SIM_PLLFLLSEL_MCGPLLCLK_CLK                       1U  /*!< PLLFLL select: MCGPLLCLK clock	*/

/**************************************** Constant definitions *********************************************/

/* Variables for BOARD_BootClockRUN configuration*/
const mcg_config_t mcgConfig_BOARD_BootClockRUN =
{
    .mcgMode = kMCG_ModePEE,					/* PEE - PLL Engaged External					*/
    .irclkEnableMode = kMCG_IrclkEnable,		/* MCGIRCLK enabled, MCGIRCLK disabled in STOP mode */
    .ircs = kMCG_IrcSlow,						/* Slow internal reference clock selected		*/
    .fcrdiv = 0x0U, 							/* Fast IRC divider: divided by 1				*/
    .frdiv = 0x0U,								/* FLL reference clock divider: divided by 32	*/
    .drs = kMCG_DrsLow, 						/* Low frequency range 							*/
    .dmx32 = kMCG_Dmx32Default,					/* DCO has a default range of 25% 				*/
    .pll0Config =
    {
        .enableMode = MCG_PLL_DISABLE,			/* MCGPLLCLK disabled 							*/
        .prdiv = 0x1U,							/* PLL Reference divider: 24/2=12M (8-16M) 		*/
        .vdiv = 0x04U,							/* VCO divider: multiplied by 20 	         	*/
    },
};
const sim_clock_config_t simConfig_BOARD_BootClockRUN =
{
    .pllFllSel = SIM_PLLFLLSEL_MCGPLLCLK_CLK,	/* PLLFLL select: MCGPLLCLK clock 				*/
    .er32kSrc = SIM_OSC32KSEL_LPO_CLK,			/* OSC32KSEL select: LPO clock 					*/
    .clkdiv1 = 0x01370000U,						/* SIM_CLKDIV1 - OUTDIV1: /1, OUTDIV2: /2, OUTDIV3: /8, OUTDIV4: /12 */
};
const osc_config_t oscConfig_BOARD_BootClockRUN =
{
    .freq = 12000000U,							/* Oscillator frequency: 12000000Hz 			*/
    .capLoad = (OSC_CAP0P), 					/* Oscillator capacity load: 0pF 				*/
    .workMode = kOSC_ModeExt,					/* Use external clock 							*/
    .oscerConfig =
    {
        .enableMode = kOSC_ErClkEnable,   		/* Enable ext ref clock, disable ext ref clock in STOP mode */
        .erclkDiv = 0,					 		/* Divider for OSCERCLK: divided by 1 			*/
    }
};

/* Variables for BOARD_BootClockHSRUN configuration*/
const mcg_config_t mcgConfig_BOARD_BootClockHSRUN =
{
	.mcgMode = kMCG_ModePEE,					/* PEE - PLL Engaged External					*/
	.irclkEnableMode = kMCG_IrclkEnable,		/* MCGIRCLK enabled, MCGIRCLK disabled in STOP mode */
	.ircs = kMCG_IrcSlow,						/* Slow internal reference clock selected		*/
	.fcrdiv = 0x0U, 							/* Fast IRC divider: divided by 1				*/
	.frdiv = 0x0U,								/* FLL reference clock divider: divided by 32	*/
	.drs = kMCG_DrsLow, 						/* Low frequency range							*/
	.dmx32 = kMCG_Dmx32Default, 				/* DCO has a default range of 25%				*/
	.pll0Config =
	{
		.enableMode = MCG_PLL_DISABLE,			/* MCGPLLCLK disabled							*/
		.prdiv = 0x1U,							/* PLL Reference divider: 24/2=12M (8-16M)		*/
		.vdiv = 0x18U,							/* VCO divider: multiplied by 40				*/
	},
};
const sim_clock_config_t simConfig_BOARD_BootClockHSRUN =
{
	.pllFllSel = SIM_PLLFLLSEL_MCGPLLCLK_CLK,	/* PLLFLL select: MCGPLLCLK clock				*/
	.er32kSrc = SIM_OSC32KSEL_LPO_CLK,			/* OSC32KSEL select: LPO clock					*/
	.clkdiv1 = 0x01390000U, 					/* SIM_CLKDIV1 - OUTDIV1: /1, OUTDIV2: /2, OUTDIV3: /8, OUTDIV4: /12 */
};
const osc_config_t oscConfig_BOARD_BootClockHSRUN =
{
	.freq = 24000000U,							/* Oscillator frequency: 24000000Hz 			*/
	.capLoad = (OSC_CAP0P), 					/* Oscillator capacity load: 0pF				*/
	.workMode = kOSC_ModeExt,					/* Use external clock							*/
	.oscerConfig =
	{
		.enableMode = kOSC_ErClkEnable, 		/* Enable ext ref clock, disable ext ref clock in STOP mode */
		.erclkDiv = 0,							/* Divider for OSCERCLK: divided by 1			*/
	}
};


/**************************************** Types used inside file *******************************************/
/*use in BOARD_InitFlash*/
flash_config_t s_flashDriver;
ftfx_cache_config_t s_cacheDriver;

uint8 resetFlag = COLD_RESET;

/*use in */
SYSCLK_PARA SysClk;

/**************************************** Static variable definitions **************************************/


/**************************************** Global variable denifitions **************************************/
/*use in BOARD_InitFlash*/
uint32_t pflashBlockBase = 0;
uint32_t pflashTotalSize = 0;
uint32_t pflashSectorSize = 0;

extern uint32_t SystemCoreClock;

static void BOARD_ConfigureHSClock(void);
static void CLOCK_CONFIG_SetSimSafeDivs(void);
static void CLOCK_CONFIG_SetFllExtRefDiv(uint8_t frdiv);
static void BOARD_ConfigureI2C();
static void BOARD_ConfigureDMA(void);
static void BOARD_ConfigureUART(void);


/**************************************** Local function declaration ***************************************/


/**************************************** Local function ***************************************************/
static void CLOCK_CONFIG_SetSimSafeDivs(void)
{
    SIM->CLKDIV1 = 0x01170000U;

	return;
}

static void CLOCK_CONFIG_SetFllExtRefDiv(uint8_t frdiv)
{
    MCG->C1 = ((MCG->C1 & ~MCG_C1_FRDIV_MASK) | MCG_C1_FRDIV(frdiv));

	return;
}

void BOARD_IdentifyStartupState()
{
	uint32 SRS0 = RCM_GetPreviousResetSources(RCM);

	if((SRS0 & RCM_SRS0_POR_MASK) || (SRS0 & RCM_SRS0_PIN_MASK))
    {
        resetFlag = COLD_RESET;      
    }
    else
    {
        resetFlag = HOT_RESET;
    }
}

/*************************************************************************************************//**
 * @brief   Setup boot hold data area with specific infomation.
 ****************************************************************************************************/
void BOARD_InitHoldData(void)
{
    HoldData.VendorID        = VENDOR_ID;
    HoldData.ProductCode     = PRODUCT_CODE;
    HoldData.Revision        = VERSION_NUMBER;		//将软硬件版本号放到RAM的修订号地址
    HoldData.SerialNumber    = SERIAL_NUMBER;

	return;
}

/****************************************************************************************************/
/*	Config FLASHN.																                    */
/*--------------------------------------------------------------------------------------------------*/
uint8 BOARD_InitFlash(void)
{
	//ftfx_security_state_t securityStatus = kFTFx_SecurityStateNotSecure; /* Return protection status*/
    status_t OperatStatus = 0;    						/* Return code from each flash driver function	*/
	uint8 Error = FALSE;
		
	/* Clean up Flash, Cache driver Structure*/
    memset(&s_flashDriver, 0, sizeof(flash_config_t));
    memset(&s_cacheDriver, 0, sizeof(ftfx_cache_config_t));

	/* Setup flash driver structure for device and initialize variables. */
    OperatStatus = FLASH_Init(&s_flashDriver);
    if (kStatus_FTFx_Success != OperatStatus)
    {
		Error = TRUE;
    }
    /* Setup flash cache driver structure for device and initialize variables. */
    OperatStatus = FTFx_CACHE_Init(&s_cacheDriver);
    if (kStatus_FTFx_Success != OperatStatus)
    {
		Error = TRUE;
    }
	
	/* Get flash properties*/
	FLASH_GetProperty(&s_flashDriver, kFLASH_PropertyPflash0BlockBaseAddr, &pflashBlockBase);
	FLASH_GetProperty(&s_flashDriver, kFLASH_PropertyPflash0TotalSize, &pflashTotalSize);
	FLASH_GetProperty(&s_flashDriver, kFLASH_PropertyPflash0SectorSize, &pflashSectorSize);
	
	return Error;
}

/****************************************************************************************************/
/*	Config RAM.																                        */
/*--------------------------------------------------------------------------------------------------*/
void BOARD_InitRAM(void)
{
    uint32_t n; 
    
    /* Addresses for VECTOR_TABLE and VECTOR_RAM come from the linker file */
#if defined(__CC_ARM)
    extern uint32_t Image$$VECTOR_ROM$$Base[];
    extern uint32_t Image$$VECTOR_RAM$$Base[];
    extern uint32_t Image$$VECTOR_RAM$$ZI$$Length[];

    #define __VECTOR_TABLE Image$$VECTOR_ROM$$Base  
    #define __VECTOR_RAM Image$$VECTOR_RAM$$Base  
    #define __RAM_VECTOR_TABLE_SIZE Image$$VECTOR_RAM$$ZI$$Length
#elif defined(__ICCARM__)
    extern uint32_t __RAM_VECTOR_TABLE_SIZE[];
    extern uint32_t __VECTOR_TABLE[];
    extern uint32_t __VECTOR_RAM[];
#elif defined(__GNUC__)
    extern uint32_t __VECTOR_TABLE[];
    extern uint32_t __VECTOR_RAM[];
    extern uint32_t __RAM_VECTOR_TABLE_SIZE_BYTES[];
    uint32_t __RAM_VECTOR_TABLE_SIZE = (uint32_t)(__RAM_VECTOR_TABLE_SIZE_BYTES);
#endif

    if (__VECTOR_RAM != __VECTOR_TABLE)
    {   
        /* Copy the vector table from ROM to RAM */
        for (n = 0; n < ((uint32_t)__RAM_VECTOR_TABLE_SIZE)/sizeof(uint32_t); n++)
        {
            __VECTOR_RAM[n] = __VECTOR_TABLE[n];
        }
        /* Point the VTOR to the position of vector table */
        SCB->VTOR = (uint32_t)__VECTOR_RAM;
    }
    else
    {
        /* Point the VTOR to the position of vector table */
        SCB->VTOR = (uint32_t)__VECTOR_TABLE;
    }

#if !defined(__CC_ARM) && !defined(__ICCARM__)
    
    /* Declare pointers for various data sections. These pointers
     * are initialized using values pulled in from the linker file */
    uint8_t * data_ram, * data_rom, * data_rom_end;
    uint8_t * bss_start, * bss_end;

    /* Get the addresses for the .data section (initialized data section) */
#if defined(__GNUC__)
    extern uint32_t __DATA_ROM[];
    extern uint32_t __DATA_RAM[];
    extern char __DATA_END[];
    data_ram = (uint8_t *)__DATA_RAM;
    data_rom = (uint8_t *)__DATA_ROM;
    data_rom_end  = (uint8_t *)__DATA_END;
    n = data_rom_end - data_rom;
#endif

    /* Copy initialized data from ROM to RAM */
    while (n--)
    {
        *data_ram++ = *data_rom++;
    }   
    
    /* Get the addresses for the .bss section (zero-initialized data) */
#if defined(__GNUC__)
    extern char __START_BSS[];
    extern char __END_BSS[];
    bss_start = (uint8_t *)__START_BSS;
    bss_end = (uint8_t *)__END_BSS;
#endif
		
    /* Clear the zero-initialized data section */
    n = bss_end - bss_start;
    while(n--)
    {
        *bss_start++ = 0;
    }
#endif /* !__CC_ARM && !__ICCARM__*/

	return;
}

/****************************************************************************************************/
/*	Config clockrun.																                */
/*--------------------------------------------------------------------------------------------------*/
void BOARD_BootClockRUN(void)
{
    /* Set the system clock dividers in SIM to safe value. */
    CLOCK_CONFIG_SetSimSafeDivs();
    /* Initializes OSC0 according to board configuration. */
    CLOCK_InitOsc0(&oscConfig_BOARD_BootClockRUN);
    CLOCK_SetXtal0Freq(oscConfig_BOARD_BootClockRUN.freq);
    /* Configure FLL external reference divider (FRDIV). */
    CLOCK_CONFIG_SetFllExtRefDiv(mcgConfig_BOARD_BootClockRUN.frdiv);
    /* Set MCG to PEE mode. */
    CLOCK_BootToPeeMode(kMCG_OscselOsc,
                        kMCG_PllClkSelPll0,
                        &mcgConfig_BOARD_BootClockRUN.pll0Config);
    /* Configure the Internal Reference clock (MCGIRCLK). */
    CLOCK_SetInternalRefClkConfig(mcgConfig_BOARD_BootClockRUN.irclkEnableMode,
                                  mcgConfig_BOARD_BootClockRUN.ircs, 
                                  mcgConfig_BOARD_BootClockRUN.fcrdiv);
    /* Set the clock configuration in SIM module. */
    CLOCK_SetSimConfig(&simConfig_BOARD_BootClockRUN);
    /* Set SystemCoreClock variable. */
    SystemCoreClock = BOARD_BOOTCLOCKRUN_CORE_CLOCK;

	SMC_SetPowerModeProtection(SMC, kSMC_AllowPowerModeAll);
    SMC_SetPowerModeRun(SMC);

	return;
}

/****************************************************************************************************/
/*	Config pins.																                    */
/*--------------------------------------------------------------------------------------------------*/
void BOARD_InitPins(void)
{
	CLOCK_EnableClock(kCLOCK_PortA);
	CLOCK_EnableClock(kCLOCK_PortB);
	CLOCK_EnableClock(kCLOCK_PortC);
	CLOCK_EnableClock(kCLOCK_PortD);
	CLOCK_EnableClock(kCLOCK_PortE);

    
    /*  UART0  */
    // PORTB0 (pin 53) is configured as UART0_RX
    PORT_SetPinMux(PORT_UART0_RX, PIN_UART0_RX, kPORT_MuxAlt7);    
    /* PORTB1 (pin 54) is configured as UART0_TX */
    PORT_SetPinMux(PORT_UART0_TX, PIN_UART0_TX, kPORT_MuxAlt7);
    
    SIM->SOPT5 = ((SIM->SOPT5 &
                   /* Mask bits to zero which are setting */
                   (~(SIM_SOPT5_UART0TXSRC_MASK | SIM_SOPT5_UART0RXSRC_MASK)))
                  
                  /* UART 0 transmit data source select: UART0_TX pin. */
                  | SIM_SOPT5_UART0TXSRC(SOPT5_UART0TXSRC_UART_TX)
                      
                      /* UART 0 receive data source select: UART0_RX pin. */
                      | SIM_SOPT5_UART0RXSRC(SOPT5_UART0RXSRC_UART_RX));
    
    /* CAN0 */ 
    // PORTB18 (pin 64) is configured as CAN0_TX
    PORT_SetPinMux(PORT_CAN0_TX, PIN_CAN0_TX, kPORT_MuxAlt2);
    
    // PORTB19 (pin 65) is configured as CAN0_RX
    PORT_SetPinMux(PORT_CAN0_RX, PIN_CAN0_RX, kPORT_MuxAlt2);      
    
#if defined(ANY_CAN_WAKE)
/****************************************************************************************************/
/*	TJA1043 pins.																                    */
/*--------------------------------------------------------------------------------------------------*/ 
    /* CAN Transfer: ERRn */
    PORT_SetPinMux(PORT_CANTRANS_ERR, PIN_CANTRANS_ERR, kPORT_MuxAsGpio);
    gpio_pin_config_t CANTansferERRConfig = {kGPIO_DigitalInput, 0};
    GPIO_PinInit(GPIO_CANTRANS_ERR, PIN_CANTRANS_ERR, &CANTansferERRConfig);
    
    /* CAN Transfer: EN */
    PORT_SetPinMux(PORT_CANTRANS_EN, PIN_CANTRANS_EN, kPORT_MuxAsGpio);
    gpio_pin_config_t CANTansferENConfig = {kGPIO_DigitalOutput, 1};
    GPIO_PinInit(GPIO_CANTRANS_EN, PIN_CANTRANS_EN, &CANTansferENConfig);
    
    /* CAN Transfer: STBn */
    PORT_SetPinMux(PORT_CANTRANS_STB, PIN_CANTRANS_STB, kPORT_MuxAsGpio);
    gpio_pin_config_t CANTansferSTBConfig = {kGPIO_DigitalOutput, 1};
    GPIO_PinInit(GPIO_CANTRANS_STB, PIN_CANTRANS_STB, &CANTansferSTBConfig);
    
    /* CAN Transfer: KL15 */
    PORT_SetPinMux(PORT_CANTRANS_KL15, PIN_CANTRANS_KL15, kPORT_MuxAsGpio);
    gpio_pin_config_t CANTansferKL15Config = {kGPIO_DigitalInput, 0};
    GPIO_PinInit(GPIO_CANTRANS_KL15, PIN_CANTRANS_KL15, &CANTansferKL15Config);
    
#elif defined(NO_CAN_WAKE)    
/****************************************************************************************************/
/*	TJA1051 pins.																                    */
/*--------------------------------------------------------------------------------------------------*/
    /* CAN Transfer: STBn */
    PORT_SetPinMux(PORT_CANTRANS_STB, PIN_CANTRANS_STB, kPORT_MuxAsGpio);
    gpio_pin_config_t CANTansferSTBConfig = {kGPIO_DigitalOutput, 0};
    GPIO_PinInit(GPIO_CANTRANS_STB, PIN_CANTRANS_STB, &CANTansferSTBConfig);
    
    /* CAN Transfer: KL15 */
    PORT_SetPinMux(PORT_CANTRANS_KL15, PIN_CANTRANS_KL15, kPORT_MuxAsGpio);
    gpio_pin_config_t CANTansferKL15Config = {kGPIO_DigitalInput, 0};
    GPIO_PinInit(GPIO_CANTRANS_KL15, PIN_CANTRANS_KL15, &CANTansferKL15Config);
    
   /* CAN Transfer: KL15CTRL */
    PORT_SetPinMux(PORT_CANTRANS_KL15_CTRL, PIN_CANTRANS_KL15_CTRL, kPORT_MuxAsGpio);
    gpio_pin_config_t CANTansferKL15CtrlConfig = {kGPIO_DigitalOutput, 1};
    GPIO_PinInit(GPIO_CANTRANS_KL15_CTRL, PIN_CANTRANS_KL15_CTRL, &CANTansferKL15CtrlConfig);
#endif

	return;
}

/****************************************************************************************************/
/*	Config DMA.																                        */
/*--------------------------------------------------------------------------------------------------*/
static void BOARD_ConfigureDMA(void)
{
	edma_config_t DmaCfg;
	
	/* Init DMAMUX */
    DMAMUX_Init(DMAMUX0);

	/* Init the EDMA module */
    EDMA_GetDefaultConfig(&DmaCfg);
    EDMA_Init(DMA0, &DmaCfg);

	return;
}

/****************************************************************************************************/
/*	Config interrupt priority group.																*/
/*--------------------------------------------------------------------------------------------------*/
void BOARD_InitSysClk(void)
{
	SysClk.CoreSysClk = CLOCK_GetFreq(kCLOCK_CoreSysClk);
	SysClk.BusClk = CLOCK_GetFreq(kCLOCK_BusClk);
	SysClk.FastPeriphClk = CLOCK_GetFreq(kCLOCK_FastPeriphClk);
	SysClk.FlashClk = CLOCK_GetFreq(kCLOCK_FlashClk);    
	SysClk.FlexBusClk = CLOCK_GetFreq(kCLOCK_FlexBusClk);

	return;
}

/****************************************************************************************************/
/*	Config interrupt priority group.																*/
/*--------------------------------------------------------------------------------------------------*/
void BOARD_ConfigIrqPriority(void)
{
    /*3bit preemptive priority, 1bit sub-prior*/
	NVIC_SetPriorityGrouping(4);

    NVIC_SetPriority(UART0_RX_TX_IRQn, 0);                     
    NVIC_SetPriority(UART0_ERR_IRQn, 1);                     
    
	/* FTM */
	NVIC_SetPriority(FTM0_IRQn, 2);
    
    /* SPI */
	NVIC_SetPriority(DMA9_DMA25_IRQn, 3);
    NVIC_SetPriority(DMA10_DMA26_IRQn, 4);

	/* CAN */
	NVIC_SetPriority(CAN0_Bus_Off_IRQn, 6);                     // CAN0 bus off
    NVIC_SetPriority(CAN0_Error_IRQn, 6);                       // CAN0 error
    NVIC_SetPriority(CAN0_Tx_Warning_IRQn, 6);                  // CAN0 transmit warning
    NVIC_SetPriority(CAN0_Rx_Warning_IRQn, 6);                  // CAN0 receive warning
    NVIC_SetPriority(CAN0_Wake_Up_IRQn, 6);                     // CAN0 wake up  
    
   return;
}

void BOARD_ConfigBootTimer(int32_t us)
{
	ftm_config_t ftmInfo;

	uint32_t Clock;
	Clock = (SysClk.FastPeriphClk / 4) / (1000000 / us);
	
	/* EtherCAT timer test */
	FTM_GetDefaultConfig(&ftmInfo);
	ftmInfo.prescale = kFTM_Prescale_Divide_4;
	FTM_Init(FTM0, &ftmInfo);
	FTM_SetTimerPeriod(FTM0, Clock);
	FTM_EnableInterrupts(FTM0, kFTM_TimeOverflowInterruptEnable);
	EnableIRQ(FTM0_IRQn);

	return;
}

void BOARD_InitCAN(void)
{
    AL_FlexCAN_Init(CAN_INSTANCE);
    AL_FlexCAN_Open(0, CAN_INSTANCE, 0);

	return;
}

void BOARD_KL15Sleep(void)
{
    GPIO_PinWrite(GPIOE, 2U, 0);
    
    return;
}

/**
 * @brief Wait watchdog access window close.
 */
static void WaitWctClose(WDOG_Type *base)
{
    /* Accessing register by bus clock */
    for (unsigned int i = 0; i < WDOG_WCT_INSTRUCITON_COUNT; i++)
    {
        (void)base->RSTCNT;
    }
}


/****************************************************************************************************/
/*  Config general DMA attributes.                                                                  */
/*--------------------------------------------------------------------------------------------------*/
/*  20181129 ZhuYuanbo: Create function.                                                            */
/****************************************************************************************************/
void BOARD_ConfigWdog(unsigned int WdogPeriod, unsigned char Enable)
{
    wdog_config_t config;

    WDOG_GetDefaultConfig(&config);
    config.enableWdog = Enable;
    config.timeoutValue = WdogPeriod * (CLOCK_GetFreq(kCLOCK_LpoClk) / 1000);
    WDOG_Init(WDOG, &config);
    WaitWctClose(WDOG);
}



/***********************************************************************
* Name	    BOARD_ConfigureHSClock
* Brief     配置时钟      
* ----------------------------------------------------------------------
* | 2022-12-22 | RuiCong | Create Function
************************************************************************/
static void BOARD_ConfigureHSClock(void)
{
    /* Set HSRUN power mode */
    SMC_SetPowerModeProtection(SMC, kSMC_AllowPowerModeAll);
    SMC_SetPowerModeHsrun(SMC);
    while (SMC_GetPowerModeState(SMC) != kSMC_PowerStateHsrun)
    {
    }
    /* Set the system clock dividers in SIM to safe value. */
    CLOCK_CONFIG_SetSimSafeDivs();
    /* Initializes OSC0 according to board configuration. */
    CLOCK_InitOsc0(&oscConfig_BOARD_BootClockHSRUN);
    CLOCK_SetXtal0Freq(oscConfig_BOARD_BootClockHSRUN.freq);
    /* Configure FLL external reference divider (FRDIV). */
    CLOCK_CONFIG_SetFllExtRefDiv(mcgConfig_BOARD_BootClockHSRUN.frdiv);
    /* Set MCG to PEE mode. */
    CLOCK_BootToPeeMode(kMCG_OscselOsc,
                        kMCG_PllClkSelPll0,
                        &mcgConfig_BOARD_BootClockHSRUN.pll0Config);
    /* Configure the Internal Reference clock (MCGIRCLK). */
    CLOCK_SetInternalRefClkConfig(mcgConfig_BOARD_BootClockHSRUN.irclkEnableMode,
                                  mcgConfig_BOARD_BootClockHSRUN.ircs, 
                                  mcgConfig_BOARD_BootClockHSRUN.fcrdiv);
    /* Set the clock configuration in SIM module. */
    CLOCK_SetSimConfig(&simConfig_BOARD_BootClockHSRUN);
    /* Set SystemCoreClock variable. */
    SystemCoreClock = BOARD_BOOTCLOCKHSRUN_CORE_CLOCK;

	return;
}


/***********************************************************************
* Name	    BOARD_ConfigureIrqPriority
* Brief     配置中断请求优先级      
* ----------------------------------------------------------------------
* | 2023-01-10 | RuiCong | Create Function
************************************************************************/
static void BOARD_ConfigureIrqPriority()
{
	
}



static void BOARD_ConfigureI2C()
{
	//I2C1_SoftwareInit();
	//I2C1_HardwareInit();
    I2C1_DMAModeInit();
    return;
}

static void BOARD_ConfigureUART()
{
    UART0_Init();
}

/***********************************************************************
* Name	    BoardConfigure
* Brief           
* ----------------------------------------------------------------------
* | 2022-12-22 | RuiCong | Create Function
************************************************************************/
void BoardConfigure()
{
    /* 开启中断 */
    EnableInterrupts;    
    
    /* 配置高速时钟 */
    BOARD_ConfigureHSClock();
    
    /* DMA配置*/
    BOARD_ConfigureDMA();
    
	/* I2C配置 */
    BOARD_ConfigureI2C();
    
    /* UART配置 */
    BOARD_ConfigureUART();
    
	return;
}

