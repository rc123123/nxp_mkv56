/****************************************************************************************************/
/*																									*/
/*	Bootloader.c	: JTx bootloader module.														*/
/*																									*/
/****************************************************************************************************/
/*																									*/
/*	 Function		: 																				*/
/*																									*/
/********************** Copyright (C) CGX Co., Ltd **************************************************/
/*																									*/
/*	Note			:	Initial version 	2018.10.18 ZhuYuanbo									*/
/*																									*/
/****************************************************************************************************/
#include "System_MKV56F24.h"
#include "fsl_device_registers.h"
#include "Basedef.h"
#include "Board.h"

#if (defined(__ICCARM__))
    #pragma section = ".data"
    #pragma section = ".data_init"
    #pragma section = ".bss"
#endif

#pragma section  = ".intvec_boot"
#pragma section  = ".flashconfig_boot"

typedef void pointer(void);
typedef void (*vector_entry)(void);
extern void __startup(void);
extern void __iar_program_start(void);

extern uint8 __boot_text_crc_start[];
extern uint8 __boot_text_crc_code[];
extern uint8 __boot_text_chkcrc_bytes[];

/****************************************************************************************************/
/*																									*/
/*		Reboot MCU																					*/
/*																									*/
/****************************************************************************************************/
void BOOT_RebootMCU(void)
{
	NVIC_SystemReset();	
	return;
}


/****************************************************************************************************/
/*																									*/
/*																									*/
/*																									*/
/****************************************************************************************************/
__attribute__ ((weak)) void SystemInitHook (void) {
  /* Void implementation of the weak function. */
}


/****************************************************************************************************/
/*																									*/
/*		SystemInit()																				*/
/*																									*/
/****************************************************************************************************/
#pragma optimize = none
void SystemInit (void) {
#if ((__FPU_PRESENT == 1) && (__FPU_USED == 1))
  SCB->CPACR |= ((3UL << 10*2) | (3UL << 11*2));    /* set CP10, CP11 Full Access */
#endif /* ((__FPU_PRESENT == 1) && (__FPU_USED == 1)) */
  
#if (DISABLE_WDOG)
  /* WDOG->UNLOCK: WDOGUNLOCK=0xC520 */
  WDOG->UNLOCK = WDOG_UNLOCK_WDOGUNLOCK(0xC520); /* Key 1 */
  /* WDOG->UNLOCK: WDOGUNLOCK=0xD928 */
  WDOG->UNLOCK = WDOG_UNLOCK_WDOGUNLOCK(0xD928); /* Key 2 */
  /* WDOG->STCTRLH: ?=0,DISTESTWDOG=0,BYTESEL=0,TESTSEL=0,TESTWDOG=0,?=0,?=1,WAITEN=1,STOPEN=1,DBGEN=0,ALLOWUPDATE=1,WINEN=0,IRQRSTEN=0,CLKSRC=1,WDOGEN=0 */
  WDOG->STCTRLH = WDOG_STCTRLH_BYTESEL(0x00) |
                 WDOG_STCTRLH_WAITEN_MASK |
                 WDOG_STCTRLH_STOPEN_MASK |
                 WDOG_STCTRLH_ALLOWUPDATE_MASK |
                 WDOG_STCTRLH_CLKSRC_MASK |
                 0x0100U;
#endif /* (DISABLE_WDOG) */

/* Enable instruction and data caches */
#if defined(__ICACHE_PRESENT) && __ICACHE_PRESENT
  SCB_EnableICache();
#endif
#if defined(__DCACHE_PRESENT) && __DCACHE_PRESENT
  SCB_EnableDCache();
#endif

  SystemInitHook();
}


/****************************************************************************************************/
/*																									*/
/*		Bootloader entry																			*/
/*																									*/
/****************************************************************************************************/
void BootMain(void)
{
//	uint16 Crc16inFile;
//	uint16 Crc16Calced;
	
	SystemInit();
	
//	/* CRC Check */
//	Crc16inFile = *(uint16*)&__boot_text_crc_code[0];
//
//	/* CRC16 code calculated by boot. */
//	CRC16_InitCcitFalse(CRC0, 0xFFFFU);
//	CRC_WriteData(CRC0, &__boot_text_crc_start[0], (uint32)__boot_text_chkcrc_bytes);
//	Crc16Calced = CRC_Get16bitResult(CRC0);
//
//	if(Crc16inFile != Crc16Calced)
//	{
//		/* Show error status using led. */
//	#if 0
//		while(1)
//		{
//			/* Stop program here. */
//		}
//	#endif
//	}
	
	/* Call IAR init library */
	__iar_program_start();
}

typedef union _FlashConfig_t
{
  int32_t Data[4];
  struct {
    uint32_t BackDoorKey[2];
    uint32_t Protection;
    uint32_t Config;
  };
} FlashConfig_t;


#pragma location = ".flashconfig_boot"
#if defined(WITH_SECURE)
__root const FlashConfig_t Config = 
{
	0xFFFFFFFF, 
	0xFFFFFFFF, 
	0xFFFFFFFF, 
	0xFFFFFBF8,
};
#else
__root const FlashConfig_t Config = 
{
	0xFFFFFFFF, 
	0xFFFFFFFF, 
	0xFFFFFFFF, 
	0xFFFFFBFA,
};
#endif
