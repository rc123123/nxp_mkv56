#include "bsp_i2c1_temp175.h"


xbool TEMP175_ReadTemperature(xfloat *currentTemperature)
{
    xint16u temp;
    xint16s tempS;
    xint8u regAddress = 0x00U;   
    xint8u tempData[2];
        
    
    /* 硬件I2C读取 */
//  I2C_ReadData(I2C1, 0x48, regAddress, tempData, 2); //TEMP175_REG_ADDR_7BIT

    /* 软件I2C读取 */
//    I2C1_Start();
//	I2C1_WriteByte(I2C1_SLAVE_ADDRESS_WRITE);
//	I2C1_WaitAck();
//	I2C1_WriteByte(regAddress);
//	I2C1_WaitAck();
//
//	I2C1_Start();
//	I2C1_WriteByte(I2C1_SLAVE_ADDRESS_READ);
//	I2C1_WaitAck();
//	tempData[0] = I2C1_ReadByte(1);
//	tempData[1] = I2C1_ReadByte(0);
//	I2C1_Stop();

	/* I2C+DMA读取 */
	I2C_DMAModeReadData(I2C1, 0x48, regAddress, tempData, 2);
    
    temp = ((tempData[0] << 4) | (tempData[1] >> 4));
    
    if(temp & 0x800)
    {
        tempS = (0xf000 | temp);
    }
    else
    {
        tempS = temp;
    }
    
    if(tempS == 0)
        return false;
    
    *currentTemperature = tempS * 0.0625;
    
    return true;
}