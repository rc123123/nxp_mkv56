#ifndef BOARDCFG_H
#define BOARDCFG_H


/*--------------------------------------------------------------------------------------------------*/
/*  Flash Security Module                                                                           */
/*--------------------------------------------------------------------------------------------------*/
#define WITH_SECURE

/*--------------------------------------------------------------------------------------------------*/
/*  Hardware version configure                                                                      */
/*--------------------------------------------------------------------------------------------------*/
#define BOARD_V100

#if defined(BOARD_V020)
    #define NO_CAN_WAKE
    #define VERSION_NUMBER                       0x00200204                //版本号(4bit-硬件 4bit-软件)
#elif defined(BOARD_V100)
    #define ANY_CAN_WAKE
    #define VERSION_NUMBER                       0x01000204                //版本号(4bit-硬件 4bit-软件)
#endif

/*--------------------------------------------------------------------------------------------------*/
/*  Product info config                                                                             */
/*--------------------------------------------------------------------------------------------------*/
#define DEVICE_NAME                         "CGXi-IB1-BOOTLOADER"
#define VENDOR_ID                           0x000C0815                //制造商标识
#define PRODUCT_CODE                        0x01000002                //产品代码
#define REVISION_NUMBER                     0xFFFFFFFF                //修订次数
#define SERIAL_NUMBER                       0x00000001                //序列号

#define S_PRODUCT_CODE                      0x01000002
#define S_VENDOR_ID                         0x000C0815
#define S_ECU_HW_NUM                        550001485                // ASCII
#define S_ECU_SW_NUM                        550001486			     // ASCII

#define DEVICE_HW_VERSION                   "v1.00  "		
#define DEVICE_SW_VERSION                   "v1.00  "				

//#define STR_ECU_HW_VERSION                  0100                     // 02.01 BCD
//#define STR_ECU_SW_VERSION                  02000001A101             // 00.00.0001.A101 mixed BCD & ASCII
//#define STR_ECU_SW_LOCAL                    "v1.01a  "               // ASCII


#define ECU_MAX                 1

/*--------------------------------------------------------------------------------------------------*/
/*  MCU config                                                                                      */
/*--------------------------------------------------------------------------------------------------*/
/* flash地址分配 */
#define ECU_BOOT_SIZE_MAX                       0x00010000              //64KB
#define ECU_PROG_SIZE_MAX                       0x00060000              //384K
#define ECU_DATA_SIZE_MAX                       0x00010000              //64KB
#define FLASH_SECTOR_BITS					    (13)
#define FLASH_SECTOR_BYTES					    (1<<FLASH_SECTOR_BITS)
#define FLASH_PROG_BYTES                        2048
#define KV56_FLASH_OFFSET_ADDR                  0x10000000
#define FLASH_DRV_BIN_BYTES                     FLASH_SECTOR_BYTES
#define ECU_CALI_DATA_ADDR                      0x10070000              //电机标定数据下载地址(flash和spiflash都是这个地址)
#define FLASH_WRITE_BYTES						256


/* spiflash地址分配 */
#define SPIFLASH_ADDR_PX_START              	0x00000000
#define SPIFLASH_ADDR_LOG_START             	0x00001000
#define SPIFLASH_ADDR_QSLOG_START           	0x00002000
#define SPIFLASH_ADDR_ECU_INFO             	 	0x00003000
#define SPIFLASH_ADDR_DTC_INFO              	0x00004000
#define SPIFLASH_ADDR_DID_INFO              	0x00005000
#define SPIFLASH_ADDR_RAC_DATA              	0x00006000
#define SPIFLASH_ADDR_MOTOR_DATA            	0x00007000

#define SPIFLASH_ECU_INFO_STORE_ADDR_START      0x00009000              //控制信息地址
#define SPIFLASH_ECU_CTRL_ADDR_START            0x00020000              //应用程序下载地址
#define SPIFLASH_ECU_DOTS_ADDR_START			0x00100000              //点序列下载地址
#define SPIFLASH_ECU_CALI_ADDR_START            0x10070000              //标定数据下载地址
#define SPIFLASH_ECU_DRV_ADDR_START             0x10000000              //驱动下载地址，实际不会下载到物理地址

#define EnableInterrupts		asm(" CPSIE i")
#define DisableInterrupts		asm(" CPSID i")

#endif