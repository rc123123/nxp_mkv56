#ifndef _BSP_I2C_H
#define _BSP_I2C_H

#include "baseDef.h"
#include "fsl_gpio.h"
#include "fsl_port.h"
#include "fsl_i2c.h"
#include "delay.h"
#include "fsl_i2c_edma.h"
#include "fsl_edma.h"
#include "fsl_dmamux.h"


#define I2C_RELEASE_BUS_COUNT 100U

#define I2C1_SDA_PORT  PORTC
#define I2C1_SCL_PORT  PORTC
#define I2C1_SDA_GPIO  GPIOC
#define I2C1_SDA_PIN   11U
#define I2C1_SCL_GPIO  GPIOC
#define I2C1_SCL_PIN   10U

#define I2C1_BAUDRATE 50000U

#define TMP175_DMA_CHANNEL 						24U

//#define I2C1_SCL(x)    do{ x ? \
//                           GPIO_PortSet(I2C1_SCL_GPIO, (1 << 10)): \     
//                           GPIO_PortClear(I2C1_SCL_GPIO, (1 << 10)); \
//                         }while(0)
//
//#define I2C1_SDA(x)    do{ x ? \
//                           GPIO_PinWrite(I2C1_SDA_GPIO, I2C1_SDA_PIN, 1): \
//                           GPIO_PinWrite(I2C1_SDA_GPIO, I2C1_SDA_PIN, 0); \
//					     }while(0)                             
                             
//#define I2C1_SCL(x)    do{ x ? \
//                           GPIO_PinWrite(I2C1_SCL_GPIO, I2C1_SCL_PIN, 1): \     
//                           GPIO_PinWrite(I2C1_SCL_GPIO, I2C1_SCL_PIN, 0); \
//                         }while(0)
//
//#define I2C1_SDA(x)    do{ x ? \
//                           GPIO_PinWrite(I2C1_SDA_GPIO, I2C1_SDA_PIN, 1): \
//                           GPIO_PinWrite(I2C1_SDA_GPIO, I2C1_SDA_PIN, 0); \
//					     }while(0)	

#define I2C1_SDA(x)   (x ? GPIO_PinWrite(I2C1_SDA_GPIO, I2C1_SDA_PIN, 1): GPIO_PinWrite(I2C1_SDA_GPIO, I2C1_SDA_PIN, 0))
#define I2C1_SCL(x)   (x ? GPIO_PinWrite(I2C1_SCL_GPIO, I2C1_SCL_PIN, 1): GPIO_PinWrite(I2C1_SCL_GPIO, I2C1_SCL_PIN, 0))

#define I2C1_SDA_IN   (GPIOC->PDDR &= ~(1 << 11))
#define I2C1_SDA_OUT  (GPIOC->PDDR |= (1 << 11))

#define I2C1_READ_PIN  GPIO_PinRead(I2C1_SDA_GPIO, I2C1_SDA_PIN)


/* I2C1需要使用的数据 */
//typedef struct _sBSP_I2C1
//{
//    
//}sBSP_I2C1;


#if defined(__cplusplus)
extern "C" {
#endif /*_cplusplus. */
    
void I2C1_SoftwareInit(void);
xint8u I2C1_WaitAck();
void I2C1_Start();
void I2C1_Stop();
void I2C1_WriteByte(xint8u data);
xint8u I2C1_ReadByte(xint8u ack);

void I2C1_HardwareInit(void);
xbool I2C_WriteData(I2C_Type *base, xint8u device_addr, xint8u reg_addr, xint8u value);
xbool I2C_ReadData(I2C_Type *base, xint8u device_addr, xint8u reg_addr, xint8u *rxBuff, xint32u rxSize);

void I2C1_DMAModeInit();
xbool I2C_DMAModeWriteData(I2C_Type *base, uint8_t deviceAddr, uint8_t regAddr, uint8_t *txBuff, uint32_t txSize);
xbool I2C_DMAModeReadData(I2C_Type *base, uint8_t deviceAddr, uint8_t regAddr, uint8_t *rxBuff, uint32_t rxSize);

/* @} */
#if defined(__cplusplus)
}
#endif /*_cplusplus. */
/*@}*/

#endif