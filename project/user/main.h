/*********************************************************************
* Copyright  CGXi Intelligent Manufacturing Wuxi Ltd.
* File       main.h
* Brief 	                                         
* --------------------------------------------------------------------
* | Date	    | Version	| Author     | Description
* | 		    |           |            |
* |             |           |            |
* | 2022-06-30  | 1.0.0		| RuiCong    | Create File
**********************************************************************/

#ifndef _MAIN_H
#define _MAIN_H

#include "board.h"
#include "delay.h"
#include "bsp_i2c1_temp175.h"

#define U8  unsigned char
#define U16 unsigned short
#define U32 unsigned int
#define U64 unsigned long long
#define S8  char
#define S16 short
#define S32 int
#define S64 long long

enum{
    COLD_RESET,
    HOT_RESET
};

typedef enum {
    RUN_APP,
    ENTER_BOOTLOADER,
    WAIT_BOOTLOADER,   
    RUN_BOOTLOADER,
    LOCAL_BOOTLOADER,
    WAIT_SKIP_UPDATE
} TASK_STATES;


extern uint32 __sys_interrupts_start[];
extern uint8 __sys_text_crc_start[];
extern uint8 __sys_text_crc_code[];
extern uint32 __sys_text_chkcrc_bytes[];
extern uint32 __data_text_start[];

void updateSkipNotify(void);

#endif /* MAIN_H */

