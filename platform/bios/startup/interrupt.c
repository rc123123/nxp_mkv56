#include "main.h"

#define BOOT_STACK_ADDRESS				(0x0000FFF8)
extern void __iar_program_start(void);
extern void __startup(void);
extern void DMA0_DMA16_DriverIRQHandler(void);
extern void DMA1_DMA17_DriverIRQHandler(void);
extern void DMA2_DMA18_DriverIRQHandler(void);
extern void DMA3_DMA19_DriverIRQHandler(void);
extern void DMA4_DMA20_DriverIRQHandler(void);
extern void DMA5_DMA21_DriverIRQHandler(void);
extern void DMA6_DMA22_DriverIRQHandler(void);
extern void DMA7_DMA23_DriverIRQHandler(void);
extern void DMA8_DMA24_DriverIRQHandler(void);
extern void DMA9_DMA25_DriverIRQHandler(void);
extern void DMA10_DMA26_DriverIRQHandler(void);
extern void DMA11_DMA27_DriverIRQHandler(void);
extern void DMA12_DMA28_DriverIRQHandler(void);
extern void I2C0_DriverIRQHandler(void);
extern void I2C1_DriverIRQHandler(void);
extern void CAN0_DriverIRQHandler(void);
extern void UART0_RX_TX_DriverIRQHandler(void);

extern volatile U32 UDS_timer1ms;

typedef void pointer(void);
typedef void (*vector_entry)(void);


volatile uint32_t IrqNum=0;
void default_isr(void)
{
	IrqNum = (*(volatile uint8_t*)(0xE000ED08));
    return;
}

void CORE_NMI(void)
{
	return;
}

void CORE_HardFault(void)
{
	return;
}

void CORE_MemMngFault(void)
{
	return;
}

void CORE_BusFault(void)
{
	return;
}

void CORE_UsageFault(void)
{
	return;
}

/****************************************************************************************************/
/*	FTM0 interrupt service routine.																	*/
/*--------------------------------------------------------------------------------------------------*/
void IRQ_FTM0(void)
{
  	/* Clear interrupt flag.*/
    FTM_ClearStatusFlags(FTM0, kFTM_TimeOverflowFlag);	

    //UDS_timer1ms++;

	/* 避免清除中断标志位未完成，再次进入中断 */
    __DSB();
    
    return;
}

void IRQ_UART0Status(void)
{
    /* 避免清除中断标志位未完成，再次进入中断 */
    __DSB();
    
    return;
}

void IRQ_UART0Error(void)
{
    /* 避免清除中断标志位未完成，再次进入中断 */
    __DSB();
    
    return;
}

#pragma section  = ".intvec_boot"
#pragma location = ".intvec_boot"
__root const vector_entry  __vector_table[256] =
{
	(pointer*)(BOOT_STACK_ADDRESS),			/* VEC0		--		Initial SP          */
	(vector_entry)(__startup),				/* VEC1		--		Initial PC          */
	CORE_NMI,								/* VEC2		--		NMI					*/
	CORE_HardFault,							/* VEC3		--		Hard fault			*/
	CORE_MemMngFault,						/* VEC4		--							*/
	CORE_BusFault,							/* VEC5		--							*/
	CORE_UsageFault,						/* VEC6		--							*/
	default_isr,							/* VEC7		--							*/
	default_isr,							/* VEC8		--							*/
	default_isr,							/* VEC9		--							*/
	default_isr,							/* VEC10	--							*/
	default_isr,							/* VEC11	--							*/
	default_isr,							/* VEC12	--							*/
	default_isr,							/* VEC13	--							*/
	default_isr,							/* VEC14	--							*/
	default_isr,							/* VEC15	--							*/
    
	DMA0_DMA16_DriverIRQHandler,		    /* VEC16	IRQ0	(SDK handler)		*/
	DMA1_DMA17_DriverIRQHandler,		    /* VEC17	IRQ1	(SDK handler)		*/
	DMA2_DMA18_DriverIRQHandler,		    /* VEC18	IRQ2	                    */
	DMA3_DMA19_DriverIRQHandler,		    /* VEC19	IRQ3                        */
	DMA4_DMA20_DriverIRQHandler,		    /* VEC20	IRQ4			            */
	DMA5_DMA21_DriverIRQHandler,		    /* VEC21	IRQ5		                */
	DMA6_DMA22_DriverIRQHandler,		    /* VEC22	IRQ6	                    */
	DMA7_DMA23_DriverIRQHandler,		    /* VEC23	IRQ7                        */
	DMA8_DMA24_DriverIRQHandler,		    /* */
	DMA9_DMA25_DriverIRQHandler,		    /* VEC25	IRQ9                        */
	DMA10_DMA26_DriverIRQHandler,		    /* VEC26	IRQ10                       */
	DMA11_DMA27_DriverIRQHandler,			/* VEC27	IRQ11                       */
	DMA12_DMA28_DriverIRQHandler,			/* VEC28	IRQ12                       */
	default_isr,							/* */
	default_isr,			                /* VEC30	IRQ14	(SDK handler)		*/
	UART0_RX_TX_DriverIRQHandler,			/* VEC31	IRQ15	(SDK handler)		*/
	
	default_isr,							/* VEC32	IRQ16	DMA ERROR			*/
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	I2C0_DriverIRQHandler,					/* VEC40    IRQ24    I2C0               */
	I2C1_DriverIRQHandler,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	IRQ_UART0Status,						    /* VEC47	IRQ31	UART0 STATUS		*/
	
	IRQ_UART0Error,							/* VEC48	IRQ32	UART0 ERROR			*/
	default_isr,//IRQ_UART1Status,					    /* VEC49	IRQ33                       */
	default_isr,//IRQ_UART1Error,							/* VEC50	IRQ34                       */
	default_isr,//IRQ_UART2Status,					    /* VEC51	IRQ35                       */
	default_isr,//IRQ_UART2Error,							/* VEC52	IRQ36                       */
	default_isr,							/* */
	default_isr,							/* VEC54	IRQ38	HSADC0/1 ERROR			*/
	default_isr,						    /* VEC55	IRQ39	HSADC0A sample finish	*/
	default_isr,							/* */
	default_isr,							/* */
	IRQ_FTM0,//IRQ_FTM0,								/* VEC58	IRQ42	FTM0					*/
	default_isr,						    /* VEC59	IRQ43	FTM1					*/
	default_isr,//IRQ_UART3Status,				        /* VEC60	IRQ44	UART3 STATUS			*/
	default_isr,//IRQ_UART3Error,							/* VEC61	IRQ45	UART3 ERROR				*/
	default_isr,//IRQ_UART4Status,						/* VEC62	IRQ46                           */
	default_isr,//IRQ_UART4Error,							/* VEC63	IRQ47                           */
	
	default_isr,//IRQ_PIT0,								/* VEC64	IRQ48	PIT0				*/
	default_isr,//IRQ_PIT1,								/* VEC65	IRQ49	PIT1				*/
	default_isr,//IRQ_PIT2,								/* VEC66	IRQ50	PIT2				*/
	default_isr,//IRQ_PIT3,								/* VEC67	IRQ51	PIT3				*/
	default_isr,						    /* VEC68	IRQ52	PDB0				*/
	default_isr,						    /* VEC69	IRQ53	FTM2				*/
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* VEC75	IRQ59	GPIOA				*/
	default_isr,							/* VEC76	IRQ60	GPIOB				*/
	default_isr,							/* VEC77	IRQ61	GPIOC				*/
	default_isr,//IRQ_GpioD,								/* VEC78	IRQ62	GPIOD				*/
	default_isr,//IRQ_GpioE,								/* VEC79	IRQ63	GPIOE				*/
	
	default_isr,							/* VEC80	IRQ64	SOFTWARE(ALL)		*/
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,						    /* VEC87	IRQ71	FTM3			    */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,						    /* VEC90	IRQ74	 HSADC1A sample finish	    */
    CAN0_DriverIRQHandler,//CAN0_DriverIRQHandler,                  /* VEC91    IRQ75    FLEXCAN0 Message buffer    */
    CAN0_DriverIRQHandler,//CAN0_DriverIRQHandler,                  /* VEC92    IRQ76    FLEXCAN0 BUS OFF           */    
    CAN0_DriverIRQHandler,//CAN0_DriverIRQHandler,                  /* VEC93    IRQ77    FLEXCAN0 ERROR             */
    CAN0_DriverIRQHandler,//CAN0_DriverIRQHandler,                  /* VEC94    IRQ78    FLEXCAN0 tx warning        */
    CAN0_DriverIRQHandler,//CAN0_DriverIRQHandler,                  /* VEC95    IRQ79    FLEXCAN0 rx warning        */

    CAN0_DriverIRQHandler,//CAN0_DriverIRQHandler,                  /* VEC96    IRQ80    FLEXCAN0 wake up           */

	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	
	default_isr,							/* VEC112	IRQ96	FLEXCAN1 ERROR		*/
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* VEC120	IRQ104	PWM1 submodule0 compare	*/
	default_isr,						    /* VEC121	IRQ105	PWM1 Reload0			*/
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	
	default_isr,							/* VEC128	IRQ112	PWM1 INPUT CAPTURE	*/
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* VEC136	IRQ120	FLEXCAN2 WAKE-UP	*/

	/* KV5x's nvic has 137 slots. */
	default_isr,							/* --									*/
	default_isr,							/* --									*/
	default_isr,							/* --									*/
	default_isr,							/* --									*/
	default_isr,							/* --									*/
	default_isr,							/* --									*/
	default_isr,							/* --									*/
	
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr,							/* */
	default_isr								/* */
};