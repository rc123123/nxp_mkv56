#include "main.h"

xfloat result[9];
uint8_t g_tipString[] =
    "Uart functional API interrupt example\r\nBoard receives characters then sends them out\r\nNow please input:\r\n";


static int FunctionAdd(int *a, int *b)
{
    return ((*a) + (*b));
}

void main()
{
    int a = 10;
    int b = 10;
    int c = 0;
    c = FunctionAdd(&a, &b);

    /* 资源配置 */
    BoardConfigure();
    
    /* Send g_tipString out. */
    UART_WriteBlocking(UART0, g_tipString, sizeof(g_tipString) / sizeof(g_tipString[0]));
    
    while(1)
    {
        TEMP175_ReadTemperature(result);
        //DelayMs(1000);
    }
}