/*********************************************************************
* Copyright  CGXi Intelligent Manufacturing Wuxi Ltd.
* File       delay.c
* Brief 	                                         
* --------------------------------------------------------------------
* | Date	    | Version	| Author     | Description
* | 		    |           |            |
* |             |           |            |
* | 2022-06-30  | 1.0.0		| RuiCong    | Create File
**********************************************************************/
#include "delay.h"
#include "main.h"




#pragma optimize = none
void DelayUs(uint32 us)
{
	uint32 delay_count = us * (SystemCoreClock / 1000000 * 2 / 5);
	for(uint32 i = 0; i < delay_count; i++)
	{
		__asm("nop");
	}
}


#pragma optimize = none
void DelayMs(uint32 ms)
{
	uint32 delay_count = ms * (SystemCoreClock / 1000 * 2 / 5);
	for(uint32 i = 0; i < delay_count; i++)
	{
		__asm("nop");
	}
}

/***********************************************************************
* Name	    OriginDelayMs
* Brief     默认时钟的延时      
* ----------------------------------------------------------------------
* | 2022-11-02 | RuiCong | Create Function
************************************************************************/
#pragma optimize = none
void OriginDelayMs(uint32 ms)
{
	uint32 delay_count = ms * 8388;
	for(uint32 i = 0; i < delay_count; i++)
	{
		__asm("nop");
	}
}
