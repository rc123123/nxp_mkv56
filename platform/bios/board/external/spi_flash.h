#ifndef SPI_FLASH_H
#define SPI_FLASH_H

#include <MKV56F24.h>
#include "fsl_dspi_edma.h"
#include "fsl_port.h"
#include "fsl_dspi.h"
#include "fsl_dmamux.h"
#include "fsl_gpio.h"
#include "boardCfg.h"
#include "delay.h"

#if defined(__cplusplus)
extern "C"{
#endif

/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define EXAMPLE_DSPI_MASTER_BASEADDR              SPI1
#define EXAMPLE_DSPI_MASTER_DMA_MUX_BASEADDR      DMAMUX
#define EXAMPLE_DSPI_MASTER_DMA_BASEADDR          DMA0
#define EXAMPLE_DSPI_MASTER_DMA_RX_REQUEST_SOURCE kDmaRequestMux0Group1SPI1Rx
#define EXAMPLE_DSPI_MASTER_DMA_TX_REQUEST_SOURCE kDmaRequestMux0Group1SPI1Tx
#define DSPI_MASTER_CLK_SRC                       DSPI1_CLK_SRC
#define DSPI_MASTER_CLK_FREQ                      CLOCK_GetFreq(DSPI1_CLK_SRC)
#define EXAMPLE_DSPI_MASTER_PCS_FOR_INIT          kDSPI_Pcs0
#define EXAMPLE_DSPI_MASTER_PCS_FOR_TRANSFER      kDSPI_MasterPcs0
#define EXAMPLE_DSPI_DEALY_COUNT                  0xfffffU
#define TRANSFER_SIZE1                            260U     /* Transfer dataSize */
#define TRANSFER_BAUDRATE                         24000000U /* Transfer baudrate - 5M */

#define SPI_FLASH_PAGE_SIZE                     256   
#define SPI_FLASH_SECTOR_SIZE                   (4096UL)
#define SPI_FLASH_TWO_SECTORS_SIZE              (8192UL)
#define SPI_FLASH_BLOCK64_SIZE                  (65536UL)

#define W25Q256                                 0XEF18  
   
#define W25X_WriteEnable                		0x06
#define W25X_WriteDisable                		0x04
#define W25X_ReadStatusReg1                		0x05
#define W25X_WriteStatusReg                		0x01
#define W25X_ReadData                           0x03
#define W25X_FastReadData                		0x0B
#define W25X_FastReadDual                		0x3B
#define W25X_PageProgram                		0x02            //256B 0.7ms
#define W25X_BlockErase                         0xD8            //64K 150ms
#define W25X_SectorErase                		0x20            //4k  50ms     
#define W25X_ChipErase                          0xC7
#define W25X_PowerDown                          0xB9
#define W25X_ReleasePowerDown       	 		0xAB
#define W25X_DeviceID                           0xAB
#define W25X_ManufactDeviceID        			0x90
#define W25X_JedecDeviceID                		0x9F   

/*MACRO DEFINITIONS FOR DMA CHANNEL*/
#define DMA_SPI_FLASH_RX_CHANNEL                25      // SPI1
#define DMA_SPI_FLASH_TX_CHANNEL                26
#define DMA_SPI_FLASH_INTERMEDIA_CHANNEL        27
   
  

void DSPI_Flash_Init(void);
unsigned short W25QXXX_Read_Page(unsigned char* pBuffer,unsigned int destAddr,unsigned short NumByteToRead);
void W25QXXX_Write_Page(unsigned char* pBuffer,unsigned int destAddr,unsigned short NumByteToWrite);
void W25QXXX_Erase_Sector(unsigned int Dst_Addr);
void W25QXXX_Erase_Block64(unsigned int destAddr); 
    
#if defined(__cplusplus)
}
#endif

#endif